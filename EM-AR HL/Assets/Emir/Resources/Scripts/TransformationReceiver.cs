﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Threading;
using UnityEngine;
using Vuforia;
public class TransformationReceiver : MonoBehaviour
{

    #region public members
    public string ipString = "192.168.0.101";
    public int port = 8052;
    #endregion

    #region private members 	
    private TcpClient socketConnection;
    private Thread clientReceiveThread;
    Matrix4x4 pointerTransformation;
    Matrix4x4 modelTransformation;
    GameObject pointer;
    GameObject emc;
    ConcurrentQueue<Matrix4x4> pointerQueue = new ConcurrentQueue<Matrix4x4>();
    ConcurrentQueue<Matrix4x4> modelQueue = new ConcurrentQueue<Matrix4x4>();


    GameObject model_container;
    GameObject model;
    #endregion

    void Start()
    {
        pointer = GameObject.Find("pointer");
        emc = GameObject.Find("marker");
        pointer.transform.parent = emc.transform;

        ConnectToTcpServer();

        model_container = GameObject.Find("skull");
        model_container.transform.parent = emc.transform;

        model = GameObject.Find("3dmodel");
        model.transform.parent = model_container.transform;

    }
    // Update is called once per frame
    void Update()
    {

        if (pointerQueue.TryDequeue(out pointerTransformation))
        {
            Quaternion r = pointerTransformation.GetRotation();
            pointer.transform.localRotation = r;
            pointer.transform.localPosition = new Vector3(pointerTransformation.m03 * 80 / 1000, pointerTransformation.m13 * 80 / 1000, pointerTransformation.m23 * 80 / 1000);
            if (pointerQueue.Count > 10) pointerQueue = new ConcurrentQueue<Matrix4x4>();
        }
        if (modelQueue.TryDequeue(out modelTransformation))
        {

            Quaternion r = modelTransformation.GetRotation();
            model_container.transform.localRotation = r;
            model_container.transform.localPosition = new Vector3(modelTransformation.m03 * 80 / 1000, modelTransformation.m13 * 80 / 1000, modelTransformation.m23 * 80 / 1000);
            if (modelQueue.Count > 10) modelQueue = new ConcurrentQueue<Matrix4x4>();
        }
    }
    
    public void PointCommand()
    {
        SendMessage("pick a point");
    }

    void OnApplicationQuit()
    {
        try
        {
            socketConnection.Close();
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log(e.Message);
        }

        // close the tcp listener
        try
        {
            clientReceiveThread.Abort();
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log(e.Message);
        }
    }
    /// <summary> 	
    /// Setup socket connection. 	
    /// </summary> 	
    private void ConnectToTcpServer()
    {
        try
        {
            clientReceiveThread = new Thread(new ThreadStart(ListenForData));
            clientReceiveThread.IsBackground = true;
            clientReceiveThread.Start();

        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log("On client connect exception " + e);
        }
    }
    /// <summary> 	
    /// Runs in background clientReceiveThread; Listens for incomming data. 	
    /// </summary>     
    private void ListenForData()
    {
        try
        {
            socketConnection = new TcpClient(ipString, port);
            Byte[] bytes = new Byte[1024];
            Matrix4x4 transformation = Matrix4x4.identity;

            while (true)
            {
                UnityEngine.Debug.Log("Connection Established!!!!");
                // Get a stream object for reading 				
                using (NetworkStream stream = socketConnection.GetStream())
                {
                    int length;
                    byte[] matrixBytes = new byte[4];
                    float[] m = new float[12];

                    // Read incomming stream into byte arrary. 					
                    while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        var incommingData = new byte[length];
                        Array.Copy(bytes, 0, incommingData, 0, length);
                        string serverMessage = Encoding.ASCII.GetString(incommingData);
                        string msg = serverMessage.Substring(5);
                        UnityEngine.Debug.Log("message received:" + msg);
                        SendMessage("client recieved message: " + msg);

                        //checkMessage(msg);

                        string[] tools = msg.Split('*');
                        for (int i = 0; i < tools.Length; i++)
                        {
                            string[] numbers = tools[i].Split('/');
                            int j = 2; // 4 bytes for msg size and and 1 for what instrument (ex: 0125/0/, means a msg size of 125 byte and instrument 0 (Constants.pointerTCPClientCode)               
                            if (numbers.Length == 14)
                            {
                                while (j < 14)
                                {
                                    m[j - 2] = float.Parse(numbers[j]);
                                    j++;
                                }

                                //Matrix4x4 transformation = new Matrix4x4();
                                transformation.SetRow(0, new Vector4(m[0], m[1], m[2], m[3]));
                                transformation.SetRow(1, new Vector4(m[4], m[5], m[6], m[7]));
                                transformation.SetRow(2, new Vector4(m[8], m[9], m[10], m[11]));
                                transformation.SetRow(3, new Vector4(0.0f, 0.0f, 0.0f, 1.0f));
                                if (numbers[1].Equals(Constants.pointerTCPClientCode))
                                {
                                    pointerQueue.Enqueue(transformation);
                                }
                                else if (numbers[1].Equals(Constants.skullTCPClientCode))
                                {
                                    modelQueue.Enqueue(transformation);
                                }
                            }
                        }

                    }

                }
            }
        }
        catch (SocketException socketException)
        {
            UnityEngine.Debug.Log("Socket exception: " + socketException);
        }
    }
    /// <summary> 	
    /// Send message to server using socket connection. 	
    /// </summary> 	
    private void SendMessage(string msg)
    {
        if (socketConnection == null)
        {
            UnityEngine.Debug.Log("socket connection is null ...");
            return;
        }
        try
        {
            // Get a stream object for writing. 			
            NetworkStream stream = socketConnection.GetStream();
            if (stream.CanWrite)
            {
                byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(msg);
                // Write byte array to socketConnection stream.                 
                stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
            }
        }
        catch (SocketException socketException)
        {
            UnityEngine.Debug.Log("Socket exception: " + socketException);
        }
    }


}



public static class MatrixExtensions
{
    public static Quaternion GetRotation(this Matrix4x4 m)
    {
        // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
        Quaternion q = new Quaternion();
        q.w = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] + m[1, 1] + m[2, 2])) / 2;
        q.x = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] - m[1, 1] - m[2, 2])) / 2;
        q.y = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] + m[1, 1] - m[2, 2])) / 2;
        q.z = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] - m[1, 1] + m[2, 2])) / 2;
        q.x *= Mathf.Sign(q.x * (m[2, 1] - m[1, 2]));
        q.y *= Mathf.Sign(q.y * (m[0, 2] - m[2, 0]));
        q.z *= Mathf.Sign(q.z * (m[1, 0] - m[0, 1]));
        return q;
    }
}

static class Constants
{
    public const float MARKER_SIZE = 80.0F; //mm
    public const string pointerTCPClientCode = "0";
    public const string skullTCPClientCode = "1";
}
