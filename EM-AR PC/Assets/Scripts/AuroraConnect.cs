﻿using UnityEngine;
using System;
using System.Net;
using System.IO;
using System.IO.Ports;
using System.Net.Sockets;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;
using UnityEditor;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Linq;
using Vuforia;

public class AuroraConnect : MonoBehaviour
{
    CalibrationAssitant calibrationAssitantScript;

    GameObject pattern2D;
    List<Vector3> PickedPointList;
    Matrix4x4 calibration;
    GameObject pointer;
    GameObject subject;
    GameObject calibrationSphere;
    AudioSource pointAddedBeep;

    public string hostname = "COM5";
    public int port = 22222;

    public List<GameObject> GameObjects;

    Connection connection;
    private CRC16 crcGenerator;
    Hashtable errors;

    Thread backgroundIOThread;
    Thread calibrationThread;
    Thread registrationThread;
    Thread txThread;

    public ConcurrentQueue<Vector3> nextCalibrationPointCoordinates = new ConcurrentQueue<Vector3>();
    private ConcurrentQueue<Matrix4x4> tipTransformations = new ConcurrentQueue<Matrix4x4>();
    private ConcurrentQueue<Matrix4x4> patientTransformations = new ConcurrentQueue<Matrix4x4>();
    private ConcurrentQueue<bool> sendBool = new ConcurrentQueue<bool>();

    private Vector3 calibPointPosition;
    private Matrix4x4 tipTransform;
    private Matrix4x4 patientTransform;

    Patient patient;

    Matrix4x4 patientRegistrationMatrix;

    LandmarkTrasnform landmarkTrasnform = new LandmarkTrasnform();

    // TCP variables
    #region private members 	
    /// <summary> 	
    /// TCPListener to listen for incomming TCP connection 	
    /// requests. 	
    /// </summary> 	
    private TcpListener tcpListener;
    /// <summary> 
    /// Background thread for TcpServer workload. 	
    /// </summary> 	
    private Thread tcpListenerThread;
    /// <summary> 	
    /// Create handle to connected tcp client. 	
    /// </summary> 	
    private TcpClient connectedTcpClient;
    #endregion
    private bool tcpOn = true;
    private string tcpMsg = "";

    int skullNumber = 1;
    int userNumber = 1;
    bool userRegPoints = false;
    private StreamWriter csvWriter;
    private StreamReader csvReader;
    string savingFilePath = @"./Assets/Resources/RegistrationMatrices/";
    float rmse = -1.0f;
    bool pickingPointOn = false;

    void Start()
    {
        sendBool.Enqueue(true);
        tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
        tcpListenerThread.IsBackground = true;
        tcpListenerThread.Start();

        errorsTable();
        crcGenerator = new CRC16();

        // Get calibration assitant
        pattern2D = GameObject.Find("2DPattern");
        calibrationAssitantScript = pattern2D.GetComponent<CalibrationAssitant>();
        PickedPointList = new List<Vector3>();
        calibration = new Matrix4x4();
        calibration = Matrix4x4.identity;
        pointer = GameObject.Find("Pointer");
        pointer.transform.parent = pattern2D.transform;
        // put your model here
        subject = GameObject.Find("Model");
        GameObjects.Add(pointer);
        GameObjects.Add(pattern2D);
        calibrationSphere = GameObject.Find("Sphere");
        calibrationSphere.transform.parent = pattern2D.transform;

        patient = new Patient();
        backgroundIOThread = new Thread((x) =>
        {
            StartupClient();
        });
        backgroundIOThread.Start();
        backgroundIOThread.Join();

        pointAddedBeep = pattern2D.AddComponent<AudioSource>();
        pointAddedBeep.clip = Resources.Load("Audio/PickedBeep") as AudioClip;
    }
    // Update is called once per frame
    private async void StartupClient()
    {
        UnityEngine.Debug.Log(hostname);
        SerialPort sp;
        if (hostname.Substring(0, 3).Equals("COM") || hostname.Substring(0, 4).Equals("/dev"))
        {
            connection = new SerialConnection(hostname);
            connection.connect(hostname);
            
            sp = ((SerialConnection)connection).getPort();

            if (connection.isConnected())
            {
                String reset;
                reset = await RESET("1");
                UnityEngine.Debug.Log("Reset the system...");
                UnityEngine.Debug.Log(reset);

                var comm = await COMM("4", "0", "0", "0", "1");
                UnityEngine.Debug.Log("Set the communication settings...");
                UnityEngine.Debug.Log(comm);
                sp.DiscardInBuffer();
                sp.BaudRate = 57600;
                Thread.Sleep(200);

                var init = await INIT();
                UnityEngine.Debug.Log("Initialize the system...");
                UnityEngine.Debug.Log(init);
                Thread.Sleep(300);
                var phsr = await PHSR("02");
                UnityEngine.Debug.Log("Assing port handles...");
                UnityEngine.Debug.Log(phsr);
                Thread.Sleep(300);
                var pinit = await PINIT("0A");
                UnityEngine.Debug.Log("Initialze 0A...");   // marker
                UnityEngine.Debug.Log(pinit);
                Thread.Sleep(300);
                var pinit2 = await PINIT("0B");
                UnityEngine.Debug.Log("Initialze 0B...");   // pointer
                UnityEngine.Debug.Log(pinit2);
                Thread.Sleep(300);
                var pinit3 = await PINIT("0C");
                UnityEngine.Debug.Log("Initialze 0C...");   // skull
                UnityEngine.Debug.Log(pinit3);
                var pena = await PENA();
                UnityEngine.Debug.Log("Enable reporting transformations of 0A...");
                UnityEngine.Debug.Log(pena);
                Thread.Sleep(100);
                var pena2 = await PENA("0B");
                UnityEngine.Debug.Log("Enable reporting transformations of 0B...");
                UnityEngine.Debug.Log(pena2);
                Thread.Sleep(100);
                var pena3 = await PENA("0C");
                UnityEngine.Debug.Log("Enable reporting transformations of 0C...");
                UnityEngine.Debug.Log(pena3);
                Thread.Sleep(100);
                var tstart = await TSTART();
                UnityEngine.Debug.Log("Start tracking mode...");
                UnityEngine.Debug.Log(tstart);
                var beep = await BEEP(2);

            }

}
        else
        {
            // Placeholder for NDI Vega connection
            // Create a new TcpConnection
            // Start TcpServer background thread 		


        }
    }

    public void OnApplicationQuit()
    {
        backgroundIOThread.Abort();
        if (connection != null)
        {
            if (connection.isConnected())
            {
                print("closing connection");
                connection.disconnect();
            }
        }
    }

    void Update()
    {
        if (tipTransformations.TryDequeue(out tipTransform))
        {
            if (tcpOn)
            { constructTCPMsg(tipTransform, Constants.pointerTCPClientCode); }

            if (Math.Abs(pointer.transform.position.x - tipTransform.m03) > 0.01 |
                            Math.Abs(pointer.transform.position.y - tipTransform.m13) > 0.01 |
                            Math.Abs(pointer.transform.position.z - tipTransform.m23) > 0.01)

            {
                Vector3 tr = new Vector3(tipTransform.GetColumn(3).x, tipTransform.GetColumn(3).y, tipTransform.GetColumn(3).z);
                pointer.transform.localPosition = tr;
                Vector3 eulerAngles = tipTransform.GetRotation().eulerAngles;
                pointer.transform.localRotation = Quaternion.Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z);
            }

        }

        if (patientTransformations.TryDequeue(out patientTransform))
        {
            if (tcpOn)
            { constructTCPMsg(patientTransform, Constants.skullTCPClientCode);}

            if (tcpOn)
            { 
                constructTCPMsg(patientTransform, Constants.skullTCPClientCode);
            }
            if (Math.Abs(subject.transform.position.x - patientTransform.m03) > 0.01 |
                        Math.Abs(subject.transform.position.y - patientTransform.m13) > 0.01 |
                        Math.Abs(subject.transform.position.z - patientTransform.m23) > 0.01)
            {
                Vector3 tr = new Vector3(patientTransform.GetColumn(3).x, patientTransform.GetColumn(3).y, patientTransform.GetColumn(3).z);
                subject.transform.localPosition = tr;
                Vector3 eulerAngles = patientTransform.GetRotation().eulerAngles;
                subject.transform.localRotation = Quaternion.Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z);
            }

        }
        if (nextCalibrationPointCoordinates.TryDequeue(out calibPointPosition))
        {
            UnityEngine.Debug.Log("CALIBPOS:" + calibPointPosition);
            pointAddedBeep.Play();
            calibrationSphere.transform.localPosition = new Vector3(calibPointPosition.x, calibPointPosition.y, calibPointPosition.z);
        }

        if (pickingPointOn || Input.GetKeyDown(KeyCode.Alpha9))
        {
            patient.clearRegPoints();
            patient.setRegistrationPointsCranio();
            UnityEngine.Debug.Log("Setting landmarks (useRegPoints = " + userRegPoints + ") for skull: " + skullNumber);

            registrationThread = new Thread((x) =>
            {
                UnityEngine.Debug.Log("Picking a point for reigstration...");
                TXRegistration("0001", 60);
            });
            registrationThread.Start();
            registrationThread.Join();

            pickingPointOn = false;
        }

        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            calibration = calibrationAssitantScript.readCalibrationMatrix("Assets/Resources/Calibration/1/calibMatS1-N1.txt");
            UnityEngine.Debug.Log("Tracking started ...");
            txThread = new Thread(async (x) =>
            {
                var tx = await TX();
            });
            txThread.Start();
            txThread.Join();
        }

        if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            UnityEngine.Debug.Log("Loading registration matrix from file ...");
            loadRegistrationMatrix(savingFilePath, skullNumber, userRegPoints);
        }

        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            calibrationThread = new Thread((x) =>
            {
                TXCalibration("0001", 60);
            });
            calibrationThread.Start();
            calibrationThread.Join();
        }

    }

    private void loadRegistrationMatrix(string filePath, int skullNumber, bool useRegistrationPoints)
    {
        string suffix;
        if (useRegistrationPoints)
            suffix = "RR";
        else
            suffix = "AN";

        string matrixfileName = filePath + "\\" + suffix + "matrix" + "_Us" + userNumber + "_Sk" + skullNumber + ".csv";

        UnityEngine.Debug.Log("Loading registration matrix from file ...");
        UnityEngine.Debug.Log(matrixfileName);

        csvReader = new StreamReader(matrixfileName);
        var headerLine = csvReader.ReadLine();
        for (int i=0; i<4; i++)
        {
            var row = csvReader.ReadLine();
            var values = row.Split(',');
            patientRegistrationMatrix.SetRow(i, new Vector4(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]), float.Parse(values[3])));
        }
        csvReader.Close();

        string rmsefileName = filePath + "\\" + suffix + "rmse" + "_Us" + userNumber + "_Sk" + skullNumber + ".csv";
        csvReader = new StreamReader(rmsefileName);
        headerLine = csvReader.ReadLine();
        rmse = float.Parse(csvReader.ReadLine());
        csvReader.Close();

        UnityEngine.Debug.Log("Loaded patient registration matrix..." + patientRegistrationMatrix);
        UnityEngine.Debug.Log("Registration RMSE = " + rmse);
    }

    private void csvFileInstanciation(string filePath, int userNumber, int skullNumber, string savedType ,bool useRegistrationPoints)
    {
        filePath = System.IO.Path.Combine(filePath, userNumber.ToString());
        System.IO.Directory.CreateDirectory(filePath);

        string suffix;
        if (useRegistrationPoints)
            suffix = "RR";
        else
            suffix = "AN";

        string fileName = filePath + "\\" + suffix + savedType + "_Us" + userNumber + "_Sk" + skullNumber + ".csv";
        csvWriter = new StreamWriter(fileName);
        if (savedType == "matrix")
        {
            UnityEngine.Debug.Log("saved matrix to: " + fileName);
            csvWriter.WriteLine("column1, column2, colum3, column4");
        }    
        else if (savedType == "rmse")
            csvWriter.WriteLine("registrationRMSE");
    }

    private void writeCSVUpdate(Matrix4x4 registrationMatrix)
    {
        csvWriter.WriteLine(registrationMatrix.m00 + "," + registrationMatrix.m01 + "," + registrationMatrix.m02 + "," + registrationMatrix.m03);
        csvWriter.WriteLine(registrationMatrix.m10 + "," + registrationMatrix.m11 + "," + registrationMatrix.m12 + "," + registrationMatrix.m13);
        csvWriter.WriteLine(registrationMatrix.m20 + "," + registrationMatrix.m21 + "," + registrationMatrix.m22 + "," + registrationMatrix.m23);
        csvWriter.WriteLine(registrationMatrix.m30 + "," + registrationMatrix.m31 + "," + registrationMatrix.m32 + "," + registrationMatrix.m33);

        csvWriter.Close();
    }

    private void writeCSVUpdate(float rmse)
    {

        csvWriter.WriteLine(rmse);
        csvWriter.Close();
    }

    private void savePoints(List<Vector3> points, string filePath, int userNumber, int skullNumber, string savedType, bool useRegistrationPoints)
    {
        filePath = System.IO.Path.Combine(filePath, userNumber.ToString());
        System.IO.Directory.CreateDirectory(filePath);

        string suffix;
        if (useRegistrationPoints)
            suffix = "RR";
        else
            suffix = "AN";

        string fileName = filePath + "\\" + suffix + savedType + "_Us" + userNumber + "_Sk" + skullNumber + ".txt";
        csvWriter = new StreamWriter(fileName);

        for (int i=0; i< points.Count(); i++)
        {
            csvWriter.WriteLine(points[i].x + " , " + points[i].y + " , " + points[i].z);
        }
        csvWriter.Close();
    }

    public void errorsTable()
    {
        // This list of error can be different from one revision to another.
        // This has been taken from Rev. 6 & Rev. 8
        errors = new Hashtable();
        errors.Add("01", "Invalid command.");
        errors.Add("02", "Command too long.");
        errors.Add("03", "Command too short.");
        errors.Add("04", "Invalid CRC calculated for command; calculated CRC does not match the one sent.");
        errors.Add("05", "Time-out on command execution.");
        errors.Add("06", "Unable to set up new communication parameters. "
            + "This occurs if one of the communication parameters is out of range.");
        errors.Add("07", "Incorrect number of parameters.");
        errors.Add("08", "Invalid port handle selected.");
        errors.Add("09", "Invalid mode selected. Either the tool tracking priority is out of range, or the tool has "
            + "sensors defined and ‘button box’ was selected.");
        errors.Add("0A", "Invalid LED selected. The LED selected is out of range.");
        errors.Add("0B", "Invalid LED state selected. The LED state selected is out of range.");
        errors.Add("0C", "Command is invalid while in the current operating mode.");
        errors.Add("0D", "No tool is assigned to the selected port handle.");
        errors.Add("0E", "Selected port handle not initialized. The port handle needs to be initialized before the "
            + "command is sent.");
        errors.Add("0F", "Selected port handle not enabled. The port handle needs to be enabled before the "
            + "command is sent.");
        errors.Add("10", "System not initialized. The system must be initialized before the command is sent.");
        errors.Add("11", "Unable to stop tracking. This occurs if there are hardware problems. Please contact NDI.");
        errors.Add("12", "Unable to start tracking. This occurs if there are hardware problems. Please contact NDI.");
        errors.Add("13", "Unable to initialize the port handle.");
        errors.Add("14", "Invalid Field Generator characterization parameters or incompatible hardware.");
        errors.Add("15", "Unable to initialize the system. This occurs if:\n"
            + "• the system could not return to Setup mode\n"
            + "• there are internal hardware problems.Please contact NDI.");
        errors.Add("16", "Unable to start Diagnostic mode. This occurs if there are hardware problems. Please contact NDI.");
        errors.Add("17", "Unable to stop Diagnostic mode. This occurs if there are hardware problems. Please contact NDI.");
        //"18" reserved.
        errors.Add("19", "Unable to read device's firmware revision information. This occurs if:\n"
            + "• the processor selected is out of range\n"
            + "• the system is unable to inquire firmware revision information from a processor");
        errors.Add("1A", "Internal system error. This occurs when the system is unable to recover after a system processing exception.");
        //"1B" - "1C" reserved.
        errors.Add("1D", "Unable to search for SROM device IDs.");
        errors.Add("1E", "Unable to read SROM device data. This occurs if the system is:\n"
            + "• unable to auto - select the first SROM device on the given port handle as a target to read from\n"
            + "• unable to read a page of SROM device data successfully");
        errors.Add("1F", "Unable to write SROM device data. This can occur if:\n"
            + "• the SROM device starting address is out of range\n"
            + "• the system is unable to auto - select the first SROM device on the given port handle\n"
            + "as a target for writing to\n"
            + "• an SROM device on the given port handle has not previously been selected with\n"
            + "the PSEL command as a target to write to\n"
            + "• the system is unable to write a page of SROM device data successfully");
        errors.Add("20", "Unable to select SROM device for given port handle and SROM device ID.");
        //"21" - "22" reserved.
        errors.Add("23", "Command parameter is out of range.");
        errors.Add("24", "Unable to select parameters by volume. This occurs if:\n"
            + "• the selected volume is not available\n"
            + "• there are internal hardware errors.Please contact NDI.");
        errors.Add("25", "Unable to determine the system’s supported features list. This occurs if the system is\n"
            + "unable to read all the hardware information.");
        //"26" - "28" reserved.
        errors.Add("29", "Main processor firmware is corrupt.");
        errors.Add("2A", "No memory is available for dynamic allocation (heap is full).");
        errors.Add("2B", "The requested port handle has not been allocated.");
        errors.Add("2C", "The requested port handle has become unoccupied.");
        errors.Add("2D", "All handles have been allocated.");
        errors.Add("2E", "Incompatible firmware versions. This can occur if:\n"
            + "• a firmware update failed\n"
            + "• components with incompatible firmware are connected\n"
            + "To correct the problem, update the firmware.");
        //"2F" - "30" reserved
        errors.Add("31", "Invalid input or output state.");
        errors.Add("32", "Invalid operation for the device associated with the specified port handle.");
        errors.Add("33", "Feature not available.");
        errors.Add("34", "User parameter does not exist.");
        errors.Add("35", "Invalid value type (e.g. string instead of integer).");
        errors.Add("36", "User parameter value set is out of valid range.");
        errors.Add("37", "User parameter array index is out of valid range.");
        errors.Add("38", "User parameter size is incorrect.");
        errors.Add("39", "Permission denied; file or user parameter is read-only.");
        errors.Add("3A", "Reply buffer too small.");
        //"3B" - "41" reserved.
        errors.Add("42", "Device not present. This occurs when the command is specific to a device that is not\n"
            + "connected to the system.");
        //"43" - "C4" reserved.
        errors.Add("C5", "The data bits parameter (set using COMM (page 29)) must be set to 8 bits in order to\n"
            + "use the BX command.");
        //"C6" - "F3" reserved.
        errors.Add("F4", "Unable to erase Flash SROM device.");
        errors.Add("F5", "Unable to write Flash SROM device.");
        errors.Add("F6", "Unable to read Flash SROM device.");
        //"F7" - "FF" reserved.

    }

    /// <summary>
    /// <c>COMM</c> - Sets the serial communication settings for the system.
    /// </summary>
    /// <param name="baudRate">0: for 9600 Bd (default) <para/ >
    /// 1: 14 400 Bd, <para/ >
    /// 2: 19 200 Bd, see usage note, <para/ >
    /// 3: 38 400 Bd, <para/ >
    /// 4: 57 600 Bd, <para/ >
    /// 5: 115 200 Bd, <para/ >
    /// 6: 921 600 Bd, see usage note <para/ >
    /// A: 230 400 Bd, see usage note </param>
    /// <param name="dataBits">default: 0 for 8bits</param>
    /// <param name="parity">default: 0 for None. 1 for Odd</param>
    /// <param name="stopBits">default 0 for 1bit. 1 for 2bits</param>
    /// <param name="handshaking">default 0 for Off. 1 for On</param>
    /// <returns></returns>
    private async Task<string> COMM(string baudRate = "0", string dataBits = "0", string parity = "0",
        string stopBits = "0", string handshaking = "0")
    {
        string response = "";
        if (connection.isConnected())
        {
            string parameters = baudRate + dataBits + parity + stopBits + handshaking;
            string command = "COMM:" + parameters;
            CRC16 crc = new CRC16();
            string crcStr = crc.StringToHexString(Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16));
            response = await connection.SendCommand(command + crcStr);
            if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
            else
                UnityEngine.Debug.Log(response);
        }
        else
            UnityEngine.Debug.Log(hostname + " not opened");
        return response;
    }


    /// <summary>
    /// <c>BEEP</c> - Sounds the system beeper.
    /// </summary>
    /// <param name="numberOfBeeps">from 1 to 9</param>
    /// <returns></returns>
    private async Task<String> BEEP(int numberOfBeeps = 1)
    {
        string response = "";
        if (connection.isConnected())
        {
            string command = "BEEP:" + numberOfBeeps;
            CRC16 crc = new CRC16();
            string crcStr = Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16);
            response = await connection.SendCommand(command + crcStr);
            if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
            else
                UnityEngine.Debug.Log(response);
        }
        else
            UnityEngine.Debug.Log(hostname + " not opened");
        return response;
    }

    /// <summary>
    /// <c>INIT</c> - Initializes the system.
    /// </summary>
    /// <returns></returns>
    private async Task<string> INIT()
    {
        string response = "";
        if (connection.isConnected())
        {
            string command = "INIT:";
            CRC16 crc = new CRC16();
            string crcStr = Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16);
            response = await connection.SendCommand(command + crcStr);
            if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
            else
                UnityEngine.Debug.Log(response);
        }
        else
            UnityEngine.Debug.Log(hostname + " not opened");
        return response;
    }

    /// <summary>
    /// <c>PENA</c> - Enables the reporting of transformations for a particular port handle.
    /// </summary>
    /// <param name="portHandle">0A to FF</param>
    /// <param name="toolTrackingPriority">S:static tool (reference tool),<para/ > 
    /// D:dynamic tool (porbe),<para/ > B:Button box.</param>
    /// <returns></returns>
    private async Task<string> PENA(string portHandle = "0A", string toolTrackingPriority = "S")
    {
        string response = "";
        if (connection.isConnected())
        {
            string parameters = portHandle + toolTrackingPriority;
            string command = "PENA:" + parameters;
            CRC16 crc = new CRC16();
            string crcStr = Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16);
            response = await connection.SendCommand(command + crcStr);
            if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
            else
                UnityEngine.Debug.Log(response);
        }
        else
            UnityEngine.Debug.Log(hostname + " not opened");
        return response;
    }

    /// <summary>
    /// <c>PHSR</c> - Returns the number of assigned port handles and the port handle status for each one. Assigns a port
    /// handle to a tool.
    /// </summary>
    /// <param name="replyOptions">
    /// 00: Reports all allocated port handles (default) <para />
    /// 01: Reports port handles that need to be freed <para />
    /// 02: Reports port handles that are occupied, but not initialized or enabled <para />
    /// 03: Reports port handles that are occupied and initialized, but not enabled <para />
    /// 04: Reports enabled port handles</param>
    /// <returns></returns>
    private async Task<string> PHSR(string replyOptions = "00")
    {
        string response = "";
        if (connection.isConnected())
        {
            string parameters = replyOptions;
            string command = "PHSR:" + parameters;
            CRC16 crc = new CRC16();
            string crcStr = Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16);
            response = await connection.SendCommand(command + crcStr);
            if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
            else
            {
                UnityEngine.Debug.Log(response);
                //ex: 020A0010B001 - 2 ports : 0A & 0B
                string numberOfPorts = response.Substring(0, 2);
                UnityEngine.Debug.Log("number of ports: " + numberOfPorts);
                if (int.Parse(numberOfPorts) > 1)
                {
                    string port1 = response.Substring(2, 2);
                    UnityEngine.Debug.Log("port 1: " + port1);
                }
            }
        }
        else
            UnityEngine.Debug.Log(hostname + " not opened");
        return response;
    }

    /// <summary>
    /// <c>PINIT</c> - Initializes a port handle.
    /// </summary>
    /// <param name="portHandle">0A to FF</param>
    /// <returns></returns>
    private async Task<string> PINIT(string portHandle = "0A")
    {
        string response = "";
        if (connection.isConnected())
        {
            string command = "PINIT:" + portHandle;
            CRC16 crc = new CRC16();
            string crcStr = Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16);
            response = await connection.SendCommand(command + crcStr);
            if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
            else
                UnityEngine.Debug.Log(response);
        }
        else
            UnityEngine.Debug.Log(hostname + " not opened");
        return response;
    }

    /// <summary>
    /// <c>RESET</c> - Resets the system.
    /// </summary>
    /// <param name="resetOptions">
    /// 0: Generates a “soft” reset and resets the baud rate to 9600. System will not beep <para/>
    /// 1: Same behaviour as resetting the system with a serial break.</param>
    /// <returns></returns>
    private async Task<string> RESET(string resetOptions = "0")
    {
        string response = "";
        if (connection.isConnected())
        {
            string command = "RESET:" + resetOptions;
            CRC16 crc = new CRC16();
            string crcStr = Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16);
            response = await connection.SendCommand(command + crcStr);
            if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
            else
                UnityEngine.Debug.Log(response);
        }
        else
            UnityEngine.Debug.Log(hostname + " not opened");
        return response;
    }

    /// <summary>
    /// <c>TSTART</c> - Starts Tracking mode.
    /// </summary>
    /// <param name="replyOption">
    /// 40: Optional, starts tracking in faster acquisition mode<para/>
    /// 80: Optional, resets the frame counter to zero</param>
    /// <returns></returns>
    private async Task<string> TSTART(string replyOption = "40")
    {
        string response = "";
        if (connection.isConnected())
        {
            string command = "TSTART:" + replyOption;
            CRC16 crc = new CRC16();
            string crcStr = Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16);
            response = await connection.SendCommand(command + crcStr);
            if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
            else
                UnityEngine.Debug.Log(response);
        }
        else
            UnityEngine.Debug.Log(hostname + " not opened");
        return response;
    }

    /// <summary>
    /// <c>TX</c> - Returns the latest tool transformations and system status information in text format.
    /// </summary>
    /// <param name="replyOption">
    /// 0001: Transformation data (default)<para/>
    /// 0800: Out-of-volume transformations</param>
    /// <returns></returns>
    public async Task<string> TX(string replyOption = "0001")
    {
        string response = "";
        bool b;

        if (connection.isConnected())
        {
            string command = "TX:" + replyOption;
            CRC16 crc = new CRC16();
            string crcStr = crc.StringToHexString(Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16));
            b = sendBool.TryDequeue(out b);
            Matrix4x4 pointerTransformation = new Matrix4x4();
            pointerTransformation = Matrix4x4.identity;
            Matrix4x4 markerTransformation = new Matrix4x4();
            markerTransformation = Matrix4x4.identity;
            Matrix4x4 patientTransformation = new Matrix4x4();
            patientTransformation = Matrix4x4.identity;

            UnityEngine.Debug.Log("patient registration matrix is ..." + patientRegistrationMatrix);

            while (b)
            {

                sendBool.Enqueue(b);
                response = await connection.SendCommand(command + crcStr);

                if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                {
                    UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
                }
                else
                {
                    response = response.Remove(0, 2);
                    string[] handles = System.Text.RegularExpressions.Regex.Split(response, "\n");
                    response.Remove(0, 2);
                    for (int i = 0; i < handles.Length - 1; i++)
                    {
                        if (handles[i].Contains("MISSIN"))
                        {
                            UnityEngine.Debug.Log("port " + handles[i][0] + handles[i][1] + " is missing.");
                            if ((handles[i][0] + handles[i][1]).Equals("0A"))
                            {
                                //Missing marker which is important for visualization
                                break;
                            }
                        }
                        else
                        {
                            Vector3 translation = new Vector3();
                            Quaternion quaternion = new Quaternion();

                            extractQuatAndTrans(handles[i], ref (translation), ref (quaternion));

                            Matrix4x4 transformation = new Matrix4x4();
                            transformation = Matrix4x4.identity;
                            transformation = quaternionToMatrix(transformation, translation, quaternion);

                            if (handles[i].Substring(0, 2).Equals("0A"))
                            {
                                // marker

                                markerTransformation = transformation;

                            }
                            else if (handles[i].Substring(0, 2).Equals("0B"))
                            {
                                // pointer

                                pointerTransformation = transformation;
                                Matrix4x4 mTransformation = markerTransformation.inverse;
                                pointerTransformation = mTransformation * pointerTransformation;

                                translation = new Vector3(-pointerTransformation.GetColumn(3).y / Constants.MARKER_SIZE, -pointerTransformation.GetColumn(3).z / Constants.MARKER_SIZE, -pointerTransformation.GetColumn(3).x / Constants.MARKER_SIZE);
                                Quaternion q;
                                q = pointerTransformation.rotation;
                                Vector3 rot = q.eulerAngles;

                                q = Quaternion.Euler(rot);
                                q = new Quaternion(q.y, q.z, q.x, q.w);
                                pointerTransformation = quaternionToMatrix(pointerTransformation, translation, q);
                                pointerTransformation = calibration * pointerTransformation;

                                tipTransformations.Enqueue(pointerTransformation);

                            }
                            else if (handles[i].Substring(0, 2).Equals("0C"))
                            {
                                // skull
                                Matrix4x4 patientSensor = new Matrix4x4();
                                patientSensor = Matrix4x4.identity;
                                Vector3 tpatient = new Vector3();
                                Quaternion qpatient = new Quaternion();
                                extractQuatAndTrans(handles[i], ref (tpatient), ref (qpatient));
                                patientSensor = quaternionToMatrix(patientSensor, tpatient, qpatient);
                                tpatient = new Vector3(-patientSensor.GetColumn(3).y, -patientSensor.GetColumn(3).z, -patientSensor.GetColumn(3).x); ;
                                qpatient = patientSensor.rotation;
                                qpatient = new Quaternion(qpatient.y, qpatient.z, qpatient.x, qpatient.w);
                                patientSensor = quaternionToMatrix(patientSensor, tpatient, qpatient);

                                Vector3 tmarker = new Vector3(-markerTransformation.GetColumn(3).y, -markerTransformation.GetColumn(3).z, -markerTransformation.GetColumn(3).x); ;
                                Quaternion qmarker = markerTransformation.rotation;
                                qmarker = new Quaternion(qmarker.y, qmarker.z, qmarker.x, qmarker.w);
                                markerTransformation = quaternionToMatrix(markerTransformation, tmarker, qmarker);
                                markerTransformation = markerTransformation.inverse;

                                patientSensor = markerTransformation * patientSensor * patientRegistrationMatrix.inverse;
                                tpatient = new Vector3(patientSensor.GetColumn(3).x / Constants.MARKER_SIZE, patientSensor.GetColumn(3).y / Constants.MARKER_SIZE, patientSensor.GetColumn(3).z / Constants.MARKER_SIZE);
                                patientSensor = quaternionToMatrix(patientSensor, tpatient, patientSensor.GetRotation());
                                patientSensor = calibration * patientSensor;

                                patientTransformations.Enqueue(patientSensor);
                            }
                            else if (handles[i].Substring(0, 2).Equals("0D"))
                            {
                                //Empty port for now
                                markerTransformation = markerTransformation.inverse;
                            }
                        }

                    }
                }
            }
        }
        else
        {
            UnityEngine.Debug.Log(hostname + " not opened");
        }

        return response;
    }

    /// <summary>
    /// <c>TXCalibration</c> - Used for picking calibration points.
    /// </summary>
    /// <param name="replyOption">
    /// 0001: Transformation data (default)<para/>
    /// 0800: Out-of-volume transformations</param>
    /// <param name="n">Number of samples to take</param>
    /// <returns></returns>
    public async void TXCalibration(string replyOption = "0801", int n = 30)
    {
        string response;
        while (n > 0)
        {
            if (connection.isConnected())
            {
                string command = "TX:" + replyOption;
                command = command.ToLower();
                CRC16 crc = new CRC16();
                string crcStr = Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16);
                response = await connection.SendCommand(command + crcStr);
                if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                    UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
                else
                {
                    UnityEngine.Debug.Log(response);
                    response = response.Remove(0, 2);
                    string[] handles = System.Text.RegularExpressions.Regex.Split(response, "\n");
                    response.Remove(0, 2);

                    Matrix4x4 pointerTransformation = new Matrix4x4();

                    Matrix4x4 markerTransformation = new Matrix4x4();

                    for (int i = 0; i < handles.Length - 1; i++)
                    {
                        if (handles[i].Contains("MISSIN"))
                        {
                            UnityEngine.Debug.Log("port " + handles[i][0] + handles[i][1] + " is missing.");
                            if ((handles[i][0] + handles[i][1]).Equals("0A") || (handles[i][0] + handles[i][1]).Equals("0B"))
                            {
                                // Missing marker or pointer which are both important for calibration
                                break;
                            }
                        }
                        else
                        {
                            Vector3 translation = new Vector3();
                            Quaternion quaternion = new Quaternion();

                            extractQuatAndTrans(handles[i], ref (translation), ref (quaternion));

                            Matrix4x4 transformation = new Matrix4x4();
                            transformation = Matrix4x4.identity;
                            transformation = quaternionToMatrix(transformation, translation, quaternion);

                            if (handles[i].Substring(0, 2).Equals("0A"))
                            {
                                // marker

                                markerTransformation = transformation;

                            }
                            else if (handles[i].Substring(0, 2).Equals("0B"))
                            {
                                // pointer

                                pointerTransformation = transformation;
                                Matrix4x4 mTransformation = markerTransformation.inverse;
                                pointerTransformation = mTransformation * pointerTransformation;

                                // EM pointer
                                translation = new Vector3(-pointerTransformation.GetColumn(3).y, -pointerTransformation.GetColumn(3).z, -pointerTransformation.GetColumn(3).x);
                                Quaternion q;
                                q = pointerTransformation.rotation;
                                Vector3 rot = q.eulerAngles;

                                q = Quaternion.Euler(rot);
                                q = new Quaternion(-q.y, -q.z, -q.x, q.w);
                                pointerTransformation = quaternionToMatrix(pointerTransformation, translation, q);

                                PickedPointList.Add(new Vector3(pointerTransformation.m03 / Constants.MARKER_SIZE, pointerTransformation.m13 / Constants.MARKER_SIZE, pointerTransformation.m23 / Constants.MARKER_SIZE));
                                UnityEngine.Debug.Log("n=" + n);
                                n--;

                            }

                        }

                    }
                }

            }
            else
                UnityEngine.Debug.Log(hostname + " not opened");

            UnityEngine.Debug.Log("HERE");

            if (n == 0) { calibrationAssitantScript.addToSourceSet(PickedPointList);}
            PickedPointList.Clear();
        }

    }

    public async void TXRegistration(string replyOption = "0001", int n = 30)
    {
        string response;
        while (n > 0)
        {
            if (connection.isConnected())
            {
                string command = "TX:" + replyOption;
                CRC16 crc = new CRC16();
                string crcStr = crc.StringToHexString(Convert.ToString(crc.ComputeChecksum(Encoding.ASCII.GetBytes(command)), 16));
                response = await connection.SendCommand(command + crcStr);

                if (response.Length >= 7 && response.Substring(0, 5).Equals("ERROR"))
                    UnityEngine.Debug.Log("ERROR" + response.Substring(5, 2) + " : " + errors[response.Substring(5, 2)]);
                else
                {
                    response = response.Remove(0, 2);
                    string[] handles = System.Text.RegularExpressions.Regex.Split(response, "\n");

                    Matrix4x4 pointerTransformation = new Matrix4x4();
                    pointerTransformation = Matrix4x4.identity;
                    Matrix4x4 markerTransformation = new Matrix4x4();
                    markerTransformation = Matrix4x4.identity;
                    Matrix4x4 patientTransformation = new Matrix4x4();
                    patientTransformation = Matrix4x4.identity;

                    for (int i = 0; i < handles.Length - 1; i++)
                    {
                        if (handles[i].Contains("MISSIN"))
                        {
                            UnityEngine.Debug.Log("port " + handles[i][0] + handles[i][1] + " is missing.");
                            if ((handles[i][0] + handles[i][1]).Equals("0A"))
                            {
                                //Missing marker which is important for visualization
                                break;
                            }
                        }
                        else
                        {
                            Vector3 translation = new Vector3();
                            Quaternion quaternion = new Quaternion();

                            extractQuatAndTrans(handles[i], ref (translation), ref (quaternion));

                            Matrix4x4 transformation = new Matrix4x4();
                            transformation = Matrix4x4.identity;
                            transformation = quaternionToMatrix(transformation, translation, quaternion);

                            if (handles[i].Substring(0, 2).Equals("0A"))
                            {
                                // marker
                                markerTransformation = transformation;

                            }

                            else if (handles[i].Substring(0, 2).Equals("0B"))       
                            {
                                // pointer
                                pointerTransformation = transformation;

                            }
                            else if (handles[i].Substring(0, 2).Equals("0C"))
                            {
                                // skull
                                patientTransformation = transformation;
                                patientTransformation = patientTransformation.inverse;
                                pointerTransformation = patientTransformation * pointerTransformation;

                                Vector3 pointPosition = new Vector3();
                                pointPosition = new Vector3(-pointerTransformation.GetColumn(3).y, -pointerTransformation.GetColumn(3).z, -pointerTransformation.GetColumn(3).x);

                                PickedPointList.Add(pointPosition);
                                n--;
                            }
                        }

                    }
                }
            }
            else
            {
                UnityEngine.Debug.Log(hostname + " not opened");
            }
        }
        UnityEngine.Debug.Log("No more points: n=" + n);

        if (n == 0)
        {
            await BEEP();
            patient.getSourcePoints().Add(landmarkTrasnform.getCentroid(PickedPointList));
            PickedPointList.Clear();
            UnityEngine.Debug.Log("Number of acquired SourcePoints: " + patient.getSourcePoints().Count);
            UnityEngine.Debug.Log("Number of CTPoints: " + patient.getRegPoints().Count);
            if (patient.getSourcePoints().Count == patient.getRegPoints().Count)
            {

                UnityEngine.Debug.Log("All points were picked (regPoints = " + userRegPoints + "): performing landmark transformations...");
                patientRegistrationMatrix = new Matrix4x4();
                savePoints(patient.getSourcePoints(), savingFilePath, userNumber, skullNumber, "BeforesourcePoints", userRegPoints);
                savePoints(patient.getRegPoints(), savingFilePath, userNumber, skullNumber, "BeforeregPoints", userRegPoints);
                patientRegistrationMatrix = landmarkTrasnform.landMarkTransform(patient.getSourcePoints(), patient.getRegPoints());

                rmse = landmarkTrasnform.getRMSE2(patientRegistrationMatrix, patient.getSourcePoints(), patient.getRegPoints());

                UnityEngine.Debug.Log("Saving patient Registration Matrix..." + patientRegistrationMatrix);
                UnityEngine.Debug.Log("Saving registration RMSE error= " + rmse);
                csvFileInstanciation(savingFilePath, userNumber, skullNumber, "matrix", userRegPoints);
                writeCSVUpdate(patientRegistrationMatrix);
                csvFileInstanciation(savingFilePath, userNumber, skullNumber, "rmse", userRegPoints);
                writeCSVUpdate(rmse);
                savePoints(patient.getSourcePoints(), savingFilePath, userNumber, skullNumber, "AfterAftersourcePoints", userRegPoints);
                savePoints(patient.getRegPoints(), savingFilePath, userNumber, skullNumber, "AfterAfterregPoints", userRegPoints);
                await BEEP(2);
                patient.clearSourcePoints();
            }
        }

        
    }

    public void extractQuatAndTrans(string response, ref Vector3 position, ref Quaternion q)
    {
        string qw = response.Substring(2, 2) + "." + response.Substring(4, 4);
        if (qw[0] == '+') qw = qw.Substring(1);
        string qx = response.Substring(8, 2) + "." + response.Substring(10, 4);
        if (qx[0] == '+') qx = qx.Substring(1);
        string qy = response.Substring(14, 2) + "." + response.Substring(16, 4);
        if (qy[0] == '+') qy = qy.Substring(1);
        string qz = response.Substring(20, 2) + "." + response.Substring(22, 4);
        if (qz[0] == '+') qz = qz.Substring(1);
        q.x = float.Parse(qx);
        q.y = float.Parse(qy);
        q.z = float.Parse(qz);
        q.w = float.Parse(qw);

        string x = response.Substring(26, 5) + "." + response.Substring(31, 2);
        if (x[0] == '+') x = x.Substring(1);
        string y = response.Substring(33, 5) + "." + response.Substring(38, 2);
        if (y[0] == '+') y = y.Substring(1);
        string z = response.Substring(40, 5) + "." + response.Substring(45, 2);
        if (z[0] == '+') z = z.Substring(1);

        position.x = float.Parse(x);
        position.y = float.Parse(y);
        position.z = float.Parse(z);
    }

    public Matrix4x4 quaternionToMatrix(Matrix4x4 transform, Vector3 position, Quaternion q)
    {
        transform = Matrix4x4.identity;
        float sqw = q.w * q.w;
        float sqx = q.x * q.x;
        float sqy = q.y * q.y;
        float sqz = q.z * q.z;
        float invs = 1 / (sqx + sqy + sqz + sqw);
        transform.m00 = (sqx - sqy - sqz + sqw) * invs; // since sqw + sqx + sqy + sqz =1/invs*invs
        transform.m11 = (-sqx + sqy - sqz + sqw) * invs;
        transform.m22 = (-sqx - sqy + sqz + sqw) * invs;
        float tmp1 = q.x * q.y;
        float tmp2 = q.z * q.w;
        transform.m10 = 2.0F * (tmp1 + tmp2) * invs;
        transform.m01 = 2.0F * (tmp1 - tmp2) * invs;

        tmp1 = q.x * q.z;
        tmp2 = q.y * q.w;
        transform.m20 = 2.0F * (tmp1 - tmp2) * invs;
        transform.m02 = 2.0F * (tmp1 + tmp2) * invs;
        tmp1 = q.y * q.z;
        tmp2 = q.x * q.w;
        transform.m21 = 2.0F * (tmp1 + tmp2) * invs;
        transform.m12 = 2.0F * (tmp1 - tmp2) * invs;

        transform.m30 = transform.m31 = transform.m32 = 0.0F;
        transform.m33 = 1.0F;

        transform.m03 = position.x;
        transform.m13 = position.y;
        transform.m23 = position.z;

        return transform;
    }

    /// <summary>
    /// Runs in background TcpServerThread; Handles incomming TcpClient requests 	
    /// </summary> 	
    private void ListenForIncommingRequests()
    {
        try
        {
            // Create listener on localhost port 8052. 			
            tcpListener = new TcpListener(IPAddress.Any, port);
            tcpListener.Start();
            UnityEngine.Debug.Log("Server is listening");
            tcpOn = true;
            Byte[] bytes = new Byte[1024];
            while (true)
            {
                using (connectedTcpClient = tcpListener.AcceptTcpClient())
                {
                    UnityEngine.Debug.Log("ACCEPTED");
                    tcpOn = true;
                    // Get a stream object for reading 
                    if (connectedTcpClient.Connected)
                    {
                        using (NetworkStream stream = connectedTcpClient.GetStream())
                        {
                            int length;
                            // Read incomming stream into byte arrary. 						
                            while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                            {
                                var incommingData = new byte[length];
                                Array.Copy(bytes, 0, incommingData, 0, length);
                                // Convert byte array to string message. 							
                                string clientMessage = Encoding.ASCII.GetString(incommingData);
                                if(length < 200)
                                {
                                    UnityEngine.Debug.Log("client message received as: " + clientMessage);
                                }
                                if (clientMessage.Equals("pick a point"))
                                {
                                    pickingPointOn = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (SocketException socketException)
        {
            UnityEngine.Debug.Log("SocketException " + socketException.ToString());
        }
    }

    private void constructTCPMsg(Matrix4x4 transformations, string p_ObjectCode)
    {
        if (connectedTcpClient == null || !connectedTcpClient.Connected)
        {
            UnityEngine.Debug.Log("Not Connected");
            tcpOn = false;
            return;
        }

        try
        {
            // Get a stream object for writing. 
            NetworkStream stream = connectedTcpClient.GetStream();

            if (stream.CanWrite)
            {
                byte[] serverMessageAsByteArray;

                string transString = p_ObjectCode + "/";
                transString += transformations.m00.ToString() + "/";
                transString += transformations.m01.ToString() + "/";
                transString += transformations.m02.ToString() + "/";
                transString += transformations.m03.ToString() + "/";
                transString += transformations.m10.ToString() + "/";
                transString += transformations.m11.ToString() + "/";
                transString += transformations.m12.ToString() + "/";
                transString += transformations.m13.ToString() + "/";
                transString += transformations.m20.ToString() + "/";
                transString += transformations.m21.ToString() + "/";
                transString += transformations.m22.ToString() + "/";
                transString += transformations.m23.ToString();

                string length = "";
                length = (transString.Length + 5).ToString();
                string lengthTmp = length;
                for (int i = length.Length; i < 4; i++)
                {
                    lengthTmp = "0" + lengthTmp;
                }

                string msg = "";
                msg += lengthTmp + "/";
                msg += transString;
                serverMessageAsByteArray = Encoding.ASCII.GetBytes(msg);

                if (tcpMsg.Equals(""))
                {
                    tcpMsg += msg;
                }
                else
                {
                    int l = Int32.Parse(lengthTmp);
                    int l2 = Int32.Parse(tcpMsg.Substring(0, 4));
                    length = (l + l2 + 6).ToString(); //6 respresents the length size of 4 byte + '/' + * to separate between tools
                    string totalSize = length;
                    for (int i = length.Length; totalSize.Length < 4; i++)
                    {
                        totalSize = "0" + totalSize;
                    }
                    tcpMsg = totalSize + "/" + tcpMsg + "*" + msg; // tools separated by *
                    serverMessageAsByteArray = Encoding.ASCII.GetBytes(tcpMsg);
                    stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);
                    tcpMsg = "";
                }
            }
        }
        catch (SocketException socketException)
        {
            UnityEngine.Debug.Log("Socket exception: " + socketException);
            bool b;
            sendBool.TryDequeue(out b);
            b = false;
            sendBool.Enqueue(b);
        }
    }
}

public class Connection
{
    public bool connected = false;
    public virtual bool isConnected() { return connected; }
    public virtual bool connect(string connectionInfo) { return false; }
    public virtual void disconnect() { }
    public async virtual Task<string> ReadResponse() { return await Task.FromResult(""); }
    public async virtual Task<string> SendCommand(string buffer) {return await Task.FromResult("");}
}

public class SerialConnection : Connection
{
    private SerialPort sp;
    private string hostname;
    public SerialConnection(string hostname)
    {
        this.hostname = hostname;
    }
    private static void DataReceivedHandler(
                        object sender,
                        SerialDataReceivedEventArgs e)
    {
        SerialPort sp = (SerialPort)sender;
        string indata = sp.ReadExisting();
        UnityEngine.Debug.Log("Data Received:");
        UnityEngine.Debug.Log(indata);
    }
    public override bool connect(string connectionInfo)
    {
        sp = new SerialPort(connectionInfo);
        sp.ReadTimeout = 5000;
        sp.WriteTimeout = 5000;
        sp.Open();
        SendSerialBreak();
        connected = sp.IsOpen;
        return connected;
    }

    public override void disconnect()
    {
        sp.Close(); connected = false;
    }

    public async override Task<string> SendCommand(string command)
    {
        if (!isConnected())
            return "";
        else
        {
            command += '\r';
            await sp.BaseStream.WriteAsync(Encoding.ASCII.GetBytes(command), 0, command.Length);
            string resp = await ReadResponse();
            return resp;
        }
    }

    public void SendSerialBreak()
    {
        sp.BreakState = true;
        Thread.Sleep(250);
        sp.BreakState = false;
    }
    public async override Task<string> ReadResponse()
    {
        string response = "";
        byte[] c = new byte[1];
        char lastChar = '\0';
        while (lastChar != '\r')
        {
            await sp.BaseStream.ReadAsync(c, 0, 1);
            lastChar = (char)c[0];
            response += lastChar;
        }
        response = response.Remove(response.Length - 1); 
        string crcString = response.Substring(response.Length - 4, 4);
        uint replyCRC16 = Convert.ToUInt32(crcString, 16);
        response = response.Remove(response.Length - 4, 4);
        CRC16 crcGenerator = new CRC16();
        uint crcValidation = crcGenerator.ComputeChecksum(System.Text.Encoding.UTF8.GetBytes(response));
        if(crcValidation != replyCRC16)
        {
            UnityEngine.Debug.Log("CRC check failed.");
        }

        return response;
    }

    public SerialPort getPort()
    {
        return sp;
    }
    public void setPort(SerialPort port)
    {
        sp = port;
    }

}

public class CRC16
{
    const ushort polynomial = 0xA001;
    ushort[] table = new ushort[256];

    public ushort ComputeChecksum(byte[] bytes)
    {
        ushort crc = 0;
        for (int i = 0; i < bytes.Length; ++i)
        {
            byte index = (byte)(crc ^ bytes[i]);
            crc = (ushort)((crc >> 8) ^ table[index]);
        }
        return crc;
    }

    public byte[] ComputeChecksumBytes(byte[] bytes)
    {
        ushort crc = ComputeChecksum(bytes);
        return BitConverter.GetBytes(crc);
    }

    public CRC16()
    {
        ushort value;
        ushort temp;
        for (ushort i = 0; i < table.Length; ++i)
        {
            value = 0;
            temp = i;
            for (byte j = 0; j < 8; ++j)
            {
                if (((value ^ temp) & 0x0001) != 0)
                {
                    value = (ushort)((value >> 1) ^ polynomial);
                }
                else
                {
                    value >>= 1;
                }
                temp >>= 1;
            }
            table[i] = value;
        }
    }

    public string StringToHexString(string inputString)
    {
        int s = inputString.Length / 4+1;
        s *= 4;
        int pad = s - inputString.Length;
        while(pad > 0)
        {
            inputString = "0" + inputString;
            pad--;
        }
        return inputString;
    }
}

public class Patient
{
    private List<Vector3> m_regPoints;
    private List<Vector3> m_sourcePoints;
    public Patient()
    {
        m_regPoints = new List<Vector3>();
        m_sourcePoints = new List<Vector3>();
    }
    public void setRegistrationPointsCranio()
    {
        // replace with landmarks of your model for point-based registration
        m_regPoints.Add(new Vector3(-0.0507f, -225.8483f, -1893.0988f));  //1
        m_regPoints.Add(new Vector3(-38.4195f, -201.0652f, -1876.6263f)); //2
        m_regPoints.Add(new Vector3(-1.5755f, -220.3548f, -1874.7789f));  //3
        m_regPoints.Add(new Vector3(36.1390f, -201.2512f, -1876.5283f));  //4
        m_regPoints.Add(new Vector3(-32.4749f, -203.1030f, -1892.4131f)); //5
        m_regPoints.Add(new Vector3(31.4574f, -204.1647f, -1890.1658f));  //6
    }
    public List<Vector3> getSourcePoints()
    {
        return m_sourcePoints;
    }
    public List<Vector3> getRegPoints()
    {
        return m_regPoints;
    }

    public void clearRegPoints()
    {
        m_regPoints.Clear();
    }
    public void clearSourcePoints()
    {
        m_sourcePoints.Clear();
    }
}
public static class MatrixExtensions
{
    public static Quaternion GetRotation(this Matrix4x4 m)
    {
        // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
        Quaternion q = new Quaternion();
        q.w = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] + m[1, 1] + m[2, 2])) / 2;
        q.x = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] - m[1, 1] - m[2, 2])) / 2;
        q.y = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] + m[1, 1] - m[2, 2])) / 2;
        q.z = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] - m[1, 1] + m[2, 2])) / 2;
        q.x *= Mathf.Sign(q.x * (m[2, 1] - m[1, 2]));
        q.y *= Mathf.Sign(q.y * (m[0, 2] - m[2, 0]));
        q.z *= Mathf.Sign(q.z * (m[1, 0] - m[0, 1]));
        return q;//Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
    }
    public static Vector3 GetTranslation(this Matrix4x4 m)
    {
        // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
        Vector3 translation = new Vector3();
        translation.x = m.m03;
        translation.y = m.m13;
        translation.z = m.m23;
        return translation;//Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
    }

    public static void GetRotationAndTranslation(this Matrix4x4 m, ref Quaternion rotation, ref Vector3 translation)
    {
        rotation = m.GetRotation();
        translation = m.GetTranslation();
    }

}
static class Constants
{
    public const float MARKER_SIZE = 80.0F; //mm
    public const string pointerTCPClientCode = "0";
    public const string skullTCPClientCode = "1";
}