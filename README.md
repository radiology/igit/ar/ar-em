## References
If you use this repository please consider citing the following articles:

1. Benmahdjoub, M., Niessen, W.J., Wolvius, E.B ., & van Walsum, T. Multimodal markers for technology-independent integration of augmented reality devices and surgical navigation systems. Virtual Reality (2022). https://doi.org/10.1007/s10055-022-00653-3
2. Thabit, A., Benmahdjoub, M., van Veelen, M. L. C., Niessen, W. J., Wolvius, E. B., & van Walsum, T. (2022). Augmented reality navigation for minimally invasive craniosynostosis surgery: a phantom study. International Journal of Computer Assisted Radiology and Surgery. https://doi.org/10.1007/s11548-022-02634-y

## Description
This repository contains the system described in the references above.
It presents an Augmented Reality (AR) surgical navigation system prototype.
The system is based on HoloLens and NDI Aurora electromagnetic (EM) tracking system.

## Requirements

### Hardware

1. [NDI Aurora](https://www.ndigital.com/electromagnetic-tracking-technology/aurora/) electromagnetic navigation system which requires:
- 1 electromagnetic coil at least (for the QR code), and a pointer to visualize an augmentation.
2. [Microsoft HoloLens 2](https://www.microsoft.com/en-us/hololens/).
3. A webcam for the desktop if the augmented reality experience is meant to be tested standalone (No HoloLens).

### Software
1. [Unity engine](https://unity.com/). 
2. [Vuforia](https://developer.vuforia.com/downloads/SDK) for Unity.
3. Vuforia image target (can be created/use the one in the resources folder).

## Content

The repository is divided in two main parts:
1. *AR-EM-PC:* this folder contains a Unity app that plays the role of a server. The server is reponsible for:
- Communicating with the NDI Aurora: receive positional and orientation information of the tracked sensors.
- Communicating with the HoloLens: send calibrated ready to use positions & orientations to the HoloLens.
- Run a standalone augmented reality app locally.
Check supplementary material in reference [[1]](#References) for a demo video.

2. *AR-EM-HL:* this is a client application. The client's job is:
- Communicating with the desktop.
- Run the augmented reality application.

## Features

The code contains the following features:
1. The communication protocol between NDI and the PC through a USB serial communication.
2. Calibration as defined in [[1]](#References).
3. Calibration assistant.
3. Image to patient registration (Require known 3D locations of landmarks in CT/MRI space).
4. Projecting the tracked pointer/phantom's models at the right orientation and location.

## Usage and workflow

### Hardware setup
Make sure you perform the following before you turn on the NDI:
1. Attach an EM sensor to the QR code. Plug it in port 0A.
2. Plug the pointer in port 0B.
3. If applicable, attach the patient/anatomy/model to augment's sensor in port 0C.

### AR-EM-PC
*AuroraConnect.cs* contains the communication protocol (with NDI and HoloLens) and the projection code.
Make sure you specify the right communication port (e.g., COM5) for the NDI
Make sure the tcp port (22222) is the same as in the AR-EM-HL app.
[](./Images/connection.png) .

*LandmarkTransform.cs* contains the point based registration algorith and some utilities like computing the RMSE.

*CalibrationAssistant.cs* contains the code needed to perform the calibration on the given image target (80x80mm Erasmus MC QR code).


- The **calibration step** is **mandatory** and needs to be performed offline:
1. Run the AR-EM-PC.
2. Wait for the system to initialize (wait for a final beep).
3. Choose one of scenarios by clicking on *F1, F2* or *F3* (e.g., F1 for 4 corner divots)
PS: You can view where to place the pointer if you point the desktop webcam towards the QR code once you click on one of the calibration buttons.
4. Click on *Keypad4* to pick a point once the pointer is placed on right location and wait for the beep.
5. Once all the points are captured, the RMSE is printed and the calibration matrices are saved in *Assets/Resources/Calibration/1/*.txt*


- The image to patient **registration** is **optional** if you only wish to visualize the augmentation of the pointer:
1. The annotated positions need to be defined in the Patient class.
2. Acquire in the same order of points that were defined in Patient class by clicking each time on *Alpha9*.
3. Once the image-to-patient registeration is over the RMSE will be printed.

- **Tracking and augmentation**:
This third step is the final one. It can be started by pushing the button *Alpha8*.

The app will augment the patient and the pointer acoordingly in the desktop app through the webcam.

If the HoloLens is connected to the desktop app, it will receive all the needed transformations to place the 3D models at the right location.


### AR-EM-HL
- Always start the AR-EM-PC Desktop app before starting the AR-EM-HL app.
- Make sure you specify the correct ip address of the desktop and the same port as defined in the AR-EM-HL running unity inspector before deploying.

The registration step as defined in the AR-EM-PC section above, can be performed by using the HoloLens. The voice command "point" can be used to acquire the divots needed instead of pushing *Alpha9*.

- The HoloLend needs to at least get a first good look on the QR code in order to show the models present in the unity scene in AR-EM-HL folder.
- Afterwards push *Alpha8* on the keyboard to run the tracking.


### Contact
Department of Radiology and Nuclear Medecine  
Biomedical Imaging Group Rotterdam

- Mohamed Benmahdjoub: m.benmahdjoub@erasmusmc.nl
- Abdullah Thabit: a.thabit@erasmusmc.nl


