﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Vuforia;
using System.Collections.Concurrent;
public class CalibrationAssitant : MonoBehaviour, ITrackableEventHandler
{
    private ConcurrentQueue<Vector3> nextCalibrationPointCoordinates = new ConcurrentQueue<Vector3>();
    private TrackableBehaviour mTrackableBehaviour;
    public int actualTargetPoint = 0;
    public LandmarkTrasnform landmakrTransform;
    public List<Vector3> sourcePoints;
    public List<Vector3> targetPoints;
    private List<Vector3> calibratedSourcePoints;
    public List<Vector3> validationSourcePoints;
    public List<Vector3> validationTargetPoints;
    public List<Vector3> pointerAccuracySourcePoints;
    private Matrix4x4 calibrationMatrix;
    private GameObject pattern2D;
    GameObject calibrator;
    public AudioSource pointAddedBeep;
    public StreamWriter writer;
    public string path = "Assets/Resources/Calibration/";
    public string calibrationNumber;
    public string pathValidation = "Assets/Validation/";
    public string scenarionN = "S1-N1";
    StreamReader reader;
    public List<Vector3> patternPoints;
    List<Vector3> boardPoints;
    List<Vector3> boardBorderPoints;
    // Start is called before the first frame update
    void Start()
    {
        
        VuforiaBehaviour.Instance.enabled = true;
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        calibrator = GameObject.Find("Calibrator");
        landmakrTransform = calibrator.GetComponent<LandmarkTrasnform>();
        calibrationMatrix = new Matrix4x4();
        sourcePoints = new List<Vector3>();
        targetPoints = new List<Vector3>();

        //pattern creaction
        patternPoints = landmakrTransform.createPatternPoints(Constants.MARKER_SIZE, 0.0F);
        boardPoints = landmakrTransform.createBoardPoints(Constants.MARKER_SIZE,-3.0F, 400.0F);
        boardBorderPoints = new List<Vector3>();
        
        validationSourcePoints = new List<Vector3>();
        validationTargetPoints = new List<Vector3>();

        addValidationPoints();
        pointerAccuracySourcePoints = new List<Vector3>();

        targetPoints.AddRange(boardBorderPoints); Debug.Log("targetPoints.Count=" + targetPoints.Count);
        // Start assitance
        // Show first targetPoint on marker
        pattern2D = GameObject.Find("2DPattern");
        pointAddedBeep = pattern2D.AddComponent<AudioSource>();
        pointAddedBeep.clip = Resources.Load("Audio/PickedBeep") as AudioClip;
        nextCalibrationPointCoordinates = GameObject.Find("Connection").GetComponent<AuroraConnect>().nextCalibrationPointCoordinates;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            //2*2 divtos
            Destroy(GameObject.Find("sphere" + actualTargetPoint));
            actualTargetPoint = 0;
            targetPoints.Clear();
            sourcePoints.Clear();
            targetPoints.AddRange(landmakrTransform.patternBorderPointExtract(patternPoints, 0, 7, 7));
            Debug.Log("N_Calibration points:" + targetPoints.Count);
            scenarionN = "S1-N1";
            writer = new StreamWriter(path + calibrationNumber + "/" + "calibMat" + scenarionN + ".txt");
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            //4*3 divtos
            Destroy(GameObject.Find("sphere" + actualTargetPoint));
            actualTargetPoint = 0;
            targetPoints.Clear();
            sourcePoints.Clear();

            targetPoints.Add(patternPoints[0]);
            targetPoints.Add(patternPoints[2]);
            targetPoints.Add(patternPoints[4]);
            targetPoints.Add(patternPoints[6]);
            targetPoints.Add(patternPoints[21]);
            targetPoints.Add(patternPoints[23]);
            targetPoints.Add(patternPoints[25]);
            targetPoints.Add(patternPoints[27]);
            targetPoints.Add(patternPoints[42]);
            targetPoints.Add(patternPoints[44]);
            targetPoints.Add(patternPoints[46]);
            targetPoints.Add(patternPoints[48]);
            Debug.Log("N_Calibration points:" + targetPoints.Count);
            scenarionN = "S1-N2";
            writer = new StreamWriter(path + calibrationNumber + "/" + "calibMat" + scenarionN + ".txt");
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            //4*6 divtos
            Destroy(GameObject.Find("sphere" + actualTargetPoint));
            actualTargetPoint = 0;
            targetPoints.Clear();
            sourcePoints.Clear();

            targetPoints.Add(patternPoints[0]);
            targetPoints.Add(patternPoints[1]);
            targetPoints.Add(patternPoints[2]);
            targetPoints.Add(patternPoints[4]);
            targetPoints.Add(patternPoints[5]);
            targetPoints.Add(patternPoints[6]);
            targetPoints.Add(patternPoints[14]);
            targetPoints.Add(patternPoints[15]);
            targetPoints.Add(patternPoints[16]);
            targetPoints.Add(patternPoints[18]);
            targetPoints.Add(patternPoints[19]);
            targetPoints.Add(patternPoints[20]);
            targetPoints.Add(patternPoints[28]);
            targetPoints.Add(patternPoints[29]);
            targetPoints.Add(patternPoints[30]);
            targetPoints.Add(patternPoints[32]);
            targetPoints.Add(patternPoints[33]);
            targetPoints.Add(patternPoints[34]);
            targetPoints.Add(patternPoints[42]);
            targetPoints.Add(patternPoints[43]);
            targetPoints.Add(patternPoints[44]);
            targetPoints.Add(patternPoints[46]);
            targetPoints.Add(patternPoints[47]);
            targetPoints.Add(patternPoints[48]);
            Debug.Log("N_Calibration points:" + targetPoints.Count);
            scenarionN = "S1-N3";
            writer = new StreamWriter(path + calibrationNumber + "/" + "calibMat" + scenarionN + ".txt");
        }
        
    }
    public void OnTrackableStateChanged(
    TrackableBehaviour.Status previousStatus,
    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
    }

    private void OnTrackingFound()
    {
        if (targetPoints.Count>0)
        {
            
            Debug.Log("COUNT:" + nextCalibrationPointCoordinates.Count);
            if (actualTargetPoint==0)
            {
                nextCalibrationPointCoordinates.Enqueue(new Vector3(targetPoints[actualTargetPoint].x, targetPoints[actualTargetPoint].y, targetPoints[actualTargetPoint].z));
            }
        }
    }
    /// <summary>
    /// Takes a sample list of the same point, computes the centroid and adds to the <c>sourcePoints</c> set.
    /// </summary>
    /// <param name="pickedPointList">A list of points acquired by the navigation system</param>
    public void addToSourceSet(List<Vector3> pickedPointList)
    {
        sourcePoints.Add(landmakrTransform.getCentroid(pickedPointList));
        if (sourcePoints.Count == targetPoints.Count)
        {
            // Compute the transformation matrix
            calibrationMatrix = landmakrTransform.landMarkTransform(sourcePoints, targetPoints);
           
            Debug.Log(calibrationMatrix);
            
            float rmse = landmakrTransform.getRMSE(calibrationMatrix, sourcePoints, targetPoints);
            writeCalibrationMatrix();
            writeSourcePoints(writer,sourcePoints);
            writeSourcePoints(writer,targetPoints);
            writeSourcePoints(writer,calibratedSourcePoints);
            writer.WriteLine("rmse=" + rmse);
            writer.Close();
            
        }
        else
        {
            
            // Show next Point on marker
            actualTargetPoint++;
            nextCalibrationPointCoordinates.Enqueue(new Vector3(targetPoints[actualTargetPoint].x, targetPoints[actualTargetPoint].y, targetPoints[actualTargetPoint].z));
            
        }
    }

    public void addValidationPoints()
    {
        validationTargetPoints.Add(boardPoints[0]);
        validationTargetPoints.Add(boardPoints[5]);
        validationTargetPoints.Add(boardPoints[10]);
        validationTargetPoints.Add(boardPoints[15]);
        validationTargetPoints.Add(boardPoints[20]);
        validationTargetPoints.Add(boardPoints[105]);
        validationTargetPoints.Add(boardPoints[110]);
        validationTargetPoints.Add(boardPoints[115]);
        validationTargetPoints.Add(boardPoints[120]);
        validationTargetPoints.Add(boardPoints[125]);
        validationTargetPoints.Add(patternPoints[21]);
        validationTargetPoints.Add(boardPoints[200]);
        validationTargetPoints.Add(boardPoints[205]);
        validationTargetPoints.Add(boardPoints[210]);
        validationTargetPoints.Add(boardPoints[215]);
        validationTargetPoints.Add(boardPoints[290]);
        validationTargetPoints.Add(boardPoints[295]);
        validationTargetPoints.Add(boardPoints[300]);
        validationTargetPoints.Add(boardPoints[305]);
        validationTargetPoints.Add(boardPoints[310]);
        validationTargetPoints.Add(boardPoints[395]);
        validationTargetPoints.Add(boardPoints[400]);
        validationTargetPoints.Add(boardPoints[405]);
        validationTargetPoints.Add(boardPoints[410]);
        validationTargetPoints.Add(boardPoints[415]);
    }


    public void setCalibratedSourcePoints(List<Vector3> calibratedSourcePoints)
    {
        this.calibratedSourcePoints = calibratedSourcePoints;
    }
    public void writeCalibrationMatrix()
    {
        for(int i=0;i<4;i++)
        {
            Vector4 row = calibrationMatrix.GetRow(i);
            writer.WriteLine(row.x.ToString("r"));
            writer.WriteLine(row.y.ToString("r"));
            writer.WriteLine(row.z.ToString("r"));
            writer.WriteLine(row.w.ToString("r"));
        }
        writer.WriteLine("---Source points coordinates---");
    }

    public void writeSourcePoints(StreamWriter writer, List<Vector3> sourcePoints)
    {
        
        foreach (var point in sourcePoints)
        {
            writer.Write("("+ point.x.ToString("r")+", ");
            writer.Write(point.y.ToString("r")+", ");
            writer.Write(point.z.ToString("r") +")");
            writer.Write("\n");
        }
        writer.WriteLine("---Target points coordinates---");
    }

    public void writeBinarySourcePoints(BinaryWriter xWriter, BinaryWriter zWriter, List<Vector3> sourcePoints)
    {
        foreach (var point in sourcePoints)
        {
            xWriter.Write(point.x);
            zWriter.Write(point.z);
        }
        xWriter.Close();
        zWriter.Close();
    }

    public Matrix4x4 readCalibrationMatrix(string path)
    {
        Matrix4x4 calibrationMatrix = new Matrix4x4();
        reader = new StreamReader(path);
        for(int i=0;i<4;i++)
        {
            Vector4 row = new Vector4();
            row.x = float.Parse(reader.ReadLine());
            row.y = float.Parse(reader.ReadLine());
            row.z = float.Parse(reader.ReadLine());
            row.w = float.Parse(reader.ReadLine());
            calibrationMatrix.SetRow(i, row);
        }
        reader.ReadLine();
        reader.Close();
        return calibrationMatrix;
    }
}
