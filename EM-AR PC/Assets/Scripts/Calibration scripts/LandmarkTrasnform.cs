﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class LandmarkTrasnform : MonoBehaviour
{
    public enum TransformationType
    {
        RIGIDBODY, AFFINE
    }
    CalibrationAssitant calibrationAssitantScript;
    GameObject pattern2D;
    public TransformationType Transformation;
    private GameObject sourceSphere;
    private GameObject sourceSphere1;
    private GameObject sourceSphere2;
    private GameObject sourceSphere3;
    private GameObject targetSphere;
    private GameObject targetSphere1;
    private GameObject targetSphere2;
    private GameObject targetSphere3;
    public Material resultMaterial;
    // Start is called before the first frame update
    void Start()
    {
        //alignmentTest();
        //pointsExtractorTest();
        pattern2D = GameObject.Find("2DPattern");
        calibrationAssitantScript = pattern2D.GetComponent<CalibrationAssitant>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public Vector3 getCentroid(List<Vector3> pointSet)
    {

        Vector3 centroid = new Vector3(0.0f, 0.0f, 0.0f);
        int numberOfPoints = pointSet.Count;
        for (int i = 0; i < numberOfPoints; i++)
        {
            centroid.x += pointSet[i].x;
            centroid.y += pointSet[i].y;
            centroid.z += pointSet[i].z;
        }
        centroid.x /= numberOfPoints;
        centroid.y /= numberOfPoints;
        centroid.z /= numberOfPoints;

        return centroid;
    }

    public float getRMSE(Matrix4x4 calibrationMatrix, List<Vector3> sourcePoints, List<Vector3> targetPoints)
    {
        float registrationError = 0.0F;
        //apply transformation
        List<Vector3> calibratedSourcePoints = new List<Vector3>();
        for (int i = 0; i < sourcePoints.Count; i++)
        {
            Debug.Log("Source point=:" + sourcePoints[i].x.ToString("r")+", "+ sourcePoints[i].y.ToString("r") + ", "+ sourcePoints[i].z.ToString("r"));
            Debug.Log("Target point=:" + targetPoints[i].x.ToString("r") + ", " + targetPoints[i].y.ToString("r") + ", " + targetPoints[i].z.ToString("r"));
            sourcePoints[i] = calibrationMatrix.MultiplyPoint(sourcePoints[i]);
            calibratedSourcePoints.Add(new Vector3(sourcePoints[i].x, sourcePoints[i].y, sourcePoints[i].z));
            Debug.Log("Calibrated point=:" + calibratedSourcePoints[i].x.ToString("r") + ", " +
                calibratedSourcePoints[i].y.ToString("r") + ", " +
                calibratedSourcePoints[i].z.ToString("r"));
        }
        calibrationAssitantScript.setCalibratedSourcePoints(calibratedSourcePoints);
        for (int i = 0;i < calibratedSourcePoints.Count;i++)
        {
            Debug.Log((Vector3.Distance(calibratedSourcePoints[i], targetPoints[i])*Constants.MARKER_SIZE).ToString("r"));
            registrationError += Vector3.Distance(calibratedSourcePoints[i], targetPoints[i])*Constants.MARKER_SIZE;
        }
        return registrationError/targetPoints.Count;
    }

    public float getRMSE2(Matrix4x4 calibrationMatrix, List<Vector3> sourcePoints, List<Vector3> targetPoints)
    {
        float registrationError = 0.0F;
        //apply transformation
        List<Vector3> calibratedSourcePoints = new List<Vector3>();
        for (int i = 0; i < sourcePoints.Count; i++)
        {
            Debug.Log("Source point=:" + sourcePoints[i].x.ToString("r") + ", " + sourcePoints[i].y.ToString("r") + ", " + sourcePoints[i].z.ToString("r"));
            Debug.Log("Target point=:" + targetPoints[i].x.ToString("r") + ", " + targetPoints[i].y.ToString("r") + ", " + targetPoints[i].z.ToString("r"));
            sourcePoints[i] = calibrationMatrix.MultiplyPoint(sourcePoints[i]);
            calibratedSourcePoints.Add(new Vector3(sourcePoints[i].x, sourcePoints[i].y, sourcePoints[i].z));
            Debug.Log("Calibrated point=:" + calibratedSourcePoints[i].x.ToString("r") + ", " +
                calibratedSourcePoints[i].y.ToString("r") + ", " +
                calibratedSourcePoints[i].z.ToString("r"));
        }
        for (int i = 0; i < calibratedSourcePoints.Count; i++)
        {
            Debug.Log((Vector3.Distance(calibratedSourcePoints[i], targetPoints[i])).ToString("r"));
            registrationError += Vector3.Distance(calibratedSourcePoints[i], targetPoints[i]);
        }
        return registrationError / targetPoints.Count;
    }

    // Original C++ implementation in vtkLandmarkTransform.cxx -------------------------------
    // --- compute the necessary transform to match the two sets of landmarks ---
    /*
      The solution is based on
      Berthold K. P. Horn (1987),
      "Closed-form solution of absolute orientation using unit quaternions,"
      Journal of the Optical Society of America A, 4:629-642
    */
    public Matrix4x4 landMarkTransform(List<Vector3> sourcePoints, List<Vector3> targetPoints)
    {
        Matrix4x4 calibration = new Matrix4x4();
        int numberOfPoints = sourcePoints.Count;

        Vector3 sourceCentroid;
        Vector3 targetCentroid;
        sourceCentroid = getCentroid(sourcePoints);
        targetCentroid = getCentroid(targetPoints);
        if (numberOfPoints == 1)
        {
            calibration = Matrix4x4.identity;
            calibration.m03 = targetCentroid[0] - sourceCentroid[0];
            calibration.m13 = targetCentroid[1] - sourceCentroid[1];
            calibration.m23 = targetCentroid[2] - sourceCentroid[2];
            return calibration;
        }


        // -- build the 3x3 matrix M --

        float[][] M = new float[3][];
        M[0] = new float[3];
        M[1] = new float[3];
        M[2] = new float[3];

        float[][] AAT = new float[3][];
        AAT[0] = new float[3];
        AAT[1] = new float[3];
        AAT[2] = new float[3];

        for (int i = 0; i < 3; i++)
        {
            AAT[i][0] = M[i][0] = 0.0F; // fill M with zeros
            AAT[i][1] = M[i][1] = 0.0F;
            AAT[i][2] = M[i][2] = 0.0F;
        }
        float[] pt = new float[3];
        float[] a = new float[3];
        float[] b = new float[3];
        float sa = 0.0F, sb = 0.0F;
        for (int i = 0; i < numberOfPoints; i++)
        {
            // get the origin-centred point (a) in the source set
            a[0] = sourcePoints[i].x; a[1] = sourcePoints[i].y; a[2] = sourcePoints[i].z;
            a[0] -= sourceCentroid[0];
            a[1] -= sourceCentroid[1];
            a[2] -= sourceCentroid[2];
            // get the origin-centred point (b) in the target set
            b[0] = targetPoints[i].x; b[1] = targetPoints[i].y; b[2] = targetPoints[i].z;
            b[0] -= targetCentroid[0];
            b[1] -= targetCentroid[1];
            b[2] -= targetCentroid[2];
            // accumulate the products a*T(b) into the matrix M
            for (int j = 0; j < 3; j++)
            {
                M[j][0] += a[j] * b[0];
                M[j][1] += a[j] * b[1];
                M[j][2] += a[j] * b[2];

                // for the affine transform, compute ((a.a^t)^-1 . a.b^t)^t.
                // a.b^t is already in M.  here we put a.a^t in AAT.
                if (Transformation == TransformationType.AFFINE)
                {
                    AAT[j][0] += a[j] * a[0];
                    AAT[j][1] += a[j] * a[1];
                    AAT[j][2] += a[j] * a[2];
                }
            }
            // accumulate scale factors (if desired)
            sa += a[0] * a[0] + a[1] * a[1] + a[2] * a[2];
            sb += b[0] * b[0] + b[1] * b[1] + b[2] * b[2];
        }

        // if source or destination is degenerate then only report
        // translation
        if (sa == 0.0F || sb == 0.0F)
        {
            calibration = Matrix4x4.identity;
            calibration[0, 3] = targetCentroid[0] - sourceCentroid[0];
            calibration[1, 3] = targetCentroid[1] - sourceCentroid[1];
            calibration[2, 3] = targetCentroid[2] - sourceCentroid[2];
            Debug.Log("2 :" + calibration);
            return calibration;
        }

        if (Transformation == TransformationType.AFFINE)
        {
            // AAT = (a.a^t)^-1
            AAT = MatrixInverse(AAT);

            // M = (a.a^t)^-1 . a.b^t
            M = MatrixProduct(AAT, M);

            // this->Matrix = M^t
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    calibration[i, j] = M[j][i];
                }
            }
        }
        else
        {
            // compute required scaling factor (if desired)
            float scale = Mathf.Sqrt(sb / sa);

            // -- build the 4x4 matrix N --

            float[][] Ndata = new float[4][];
            Ndata[0] = new float[4];
            Ndata[1] = new float[4];
            Ndata[2] = new float[4];
            Ndata[3] = new float[4];

            float[][] N = new float[4][];
            N[0] = new float[4];
            N[1] = new float[4];
            N[2] = new float[4];
            N[3] = new float[4];
            for (int i = 0; i < 4; i++)
            {
                N[i] = Ndata[i];
                N[i][0] = 0.0F; // fill N with zeros
                N[i][1] = 0.0F;
                N[i][2] = 0.0F;
                N[i][3] = 0.0F;
            }
            // on-diagonal elements
            N[0][0] = M[0][0] + M[1][1] + M[2][2];
            N[1][1] = M[0][0] - M[1][1] - M[2][2];
            N[2][2] = -M[0][0] + M[1][1] - M[2][2];
            N[3][3] = -M[0][0] - M[1][1] + M[2][2];
            // off-diagonal elements
            N[0][1] = N[1][0] = M[1][2] - M[2][1];
            N[0][2] = N[2][0] = M[2][0] - M[0][2];
            N[0][3] = N[3][0] = M[0][1] - M[1][0];

            N[1][2] = N[2][1] = M[0][1] + M[1][0];
            N[1][3] = N[3][1] = M[2][0] + M[0][2];
            N[2][3] = N[3][2] = M[1][2] + M[2][1];


            // -- eigen-decompose N (is symmetric) --

            float[][] eigenvectorData = new float[4][];
            eigenvectorData[0] = new float[4];
            eigenvectorData[1] = new float[4];
            eigenvectorData[2] = new float[4];
            eigenvectorData[3] = new float[4];

            float[][] eigenvectors = new float[4][];
            eigenvectors[0] = new float[4];
            eigenvectors[1] = new float[4];
            eigenvectors[2] = new float[4];
            eigenvectors[3] = new float[4];

            float[] eigenvalues = new float[4];

            eigenvectors[0] = eigenvectorData[0];
            eigenvectors[1] = eigenvectorData[1];
            eigenvectors[2] = eigenvectorData[2];
            eigenvectors[3] = eigenvectorData[3];

            JacobiN(N, 4, eigenvalues, eigenvectors);

            // the eigenvector with the largest eigenvalue is the quaternion we want
            // (they are sorted in decreasing order for us by JacobiN)
            float w, x, y, z;
            // first: if points are collinear, choose the quaternion that
            // results in the smallest rotation.
            if (eigenvalues[0] == eigenvalues[1] || numberOfPoints == 2)
            {
                float[] s0 = new float[3];
                float[] t0 = new float[3];
                float[] s1 = new float[3];
                float[] t1 = new float[3];

                s0[0] = sourcePoints[0].x; s0[1] = sourcePoints[0].y; s0[2] = sourcePoints[0].z;
                t0[0] = targetPoints[0].x; t0[1] = targetPoints[0].y; t0[2] = targetPoints[0].z;
                s1[0] = sourcePoints[1].x; s1[1] = sourcePoints[1].y; s1[2] = sourcePoints[1].z;
                t1[0] = targetPoints[1].x; t1[1] = targetPoints[1].y; t1[2] = targetPoints[1].z;



                float[] ds = new float[3];
                float[] dt = new float[3];
                float rs = 0, rt = 0;
                for (int i = 0; i < 3; i++)
                {
                    ds[i] = s1[i] - s0[i];      // vector between points
                    rs += ds[i] * ds[i];
                    dt[i] = t1[i] - t0[i];
                    rt += dt[i] * dt[i];
                }

                // normalize the two vectors
                rs = Mathf.Sqrt(rs);
                ds[0] /= rs; ds[1] /= rs; ds[2] /= rs;
                rt = Mathf.Sqrt(rt);
                dt[0] /= rt; dt[1] /= rt; dt[2] /= rt;

                // take dot & cross product
                w = ds[0] * dt[0] + ds[1] * dt[1] + ds[2] * dt[2];
                x = ds[1] * dt[2] - ds[2] * dt[1];
                y = ds[2] * dt[0] - ds[0] * dt[2];
                z = ds[0] * dt[1] - ds[1] * dt[0];

                float r = Mathf.Sqrt(x * x + y * y + z * z);
                float theta = Mathf.Atan2(r, w);

                // construct quaternion
                w = Mathf.Cos(theta / 2);
                if (r != 0.0F)
                {
                    Debug.Log("construct quaternion");
                    r = Mathf.Sin(theta / 2) / r;
                    x = x * r;
                    y = y * r;
                    z = z * r;
                }
                else // rotation by 180 degrees: special case
                {
                    Debug.Log("rotation by 180 degrees: special case");
                    // rotate around a vector perpendicular to ds
                    Perpendiculars(ds, dt, null, 0);
                    r = Mathf.Sin(theta / 2);
                    x = dt[0] * r;
                    y = dt[1] * r;
                    z = dt[2] * r;
                }
            }
            else // points are not collinear
            {
                Debug.Log("points are not collinear");
                w = eigenvectors[0][0];
                x = eigenvectors[1][0];
                y = eigenvectors[2][0];
                z = eigenvectors[3][0];
            }

            // convert quaternion to a rotation matrix
            Debug.Log("x,y,z,w = " + x + ", " + y + ", " + z + ", " + w);
            float ww = w * w; 
            float wx = w * x;
            float wy = w * y;
            float wz = w * z;

            float xx = x * x;
            float yy = y * y; 
            float zz = z * z; 

            float xy = x * y;
            float xz = x * z;
            float yz = y * z;



            calibration[0, 0] = ww + xx - yy - zz;
            calibration[1, 0] = 2.0F * (wz + xy);
            calibration[2, 0] = 2.0F * (-wy + xz);

            calibration[0, 1] = 2.0F * (-wz + xy);
            calibration[1, 1] = ww - xx + yy - zz;
            calibration[2, 1] = 2.0F * (wx + yz);

            calibration[0, 2] = 2.0F * (wy + xz);
            calibration[1, 2] = 2.0F * (-wx + yz);
            calibration[2, 2] = ww - xx - yy + zz;

            if (Transformation != TransformationType.RIGIDBODY)
            { // add in the scale factor (if desired)
                for (int i = 0; i < 3; i++)
                {
                    calibration[i, 0] *= scale;
                    calibration[i, 1] *= scale;
                    calibration[i, 2] *= scale;
                }
            }
        }

        // the translation is given by the difference in the transformed source
        // centroid and the target centroid
        float sx, sy, sz;

        sx = calibration[0, 0] * sourceCentroid[0] +
             calibration[0, 1] * sourceCentroid[1] +
             calibration[0, 2] * sourceCentroid[2];

        sy = calibration[1, 0] * sourceCentroid[0] +
             calibration[1, 1] * sourceCentroid[1] +
             calibration[1, 2] * sourceCentroid[2];

        sz = calibration[2, 0] * sourceCentroid[0] +
             calibration[2, 1] * sourceCentroid[1] +
             calibration[2, 2] * sourceCentroid[2];

        calibration[0, 3] = targetCentroid[0] - sx;
        calibration[1, 3] = targetCentroid[1] - sy;
        calibration[2, 3] = targetCentroid[2] - sz;

        // fill the bottom row of the 4x4 matrix
        calibration[3, 0] = 0.0F;
        calibration[3, 1] = 0.0F;
        calibration[3, 2] = 0.0F;
        calibration[3, 3] = 1.0F;

        return calibration;
    }

    // Original C++ implementation in vtkMath.cxx --------------------------------------------------
    // Jacobi iteration for the solution of eigenvectors/eigenvalues of a 3x3
    // real symmetric matrix. Square 3x3 matrix a; output eigenvalues in w;
    // and output eigenvectors in v. Resulting eigenvalues/vectors are sorted
    // in decreasing order; eigenvectors are normalized.
    static bool JacobiN(float[][] a, int n, float[] w, float[][] v)
    {
        int i, j, k, iq, ip, numPos;
        float tresh, theta, tau, t, sm, s, h, g, c, tmp;
        float[] bspace = new float[4]; float[] zspace = new float[4];
        float[] b = bspace;
        float[] z = zspace;

        // only allocate memory if the matrix is large
        if (n > 4)
        {
            b = new float[n];
            z = new float[n];
        }

        // initialize
        for (ip = 0; ip < n; ip++)
        {
            for (iq = 0; iq < n; iq++)
            {
                v[ip][iq] = 0.0F;
            }
            v[ip][ip] = 1.0F;
        }
        for (ip = 0; ip < n; ip++)
        {
            b[ip] = w[ip] = a[ip][ip];
            z[ip] = 0.0F;
        }

        // begin rotation sequence
        for (i = 0; i < 20; ++i)
        {
            sm = 0.0F;
            for (ip = 0; ip < n - 1; ip++)
            {
                for (iq = ip + 1; iq < n; iq++)
                {
                    sm += Mathf.Abs(a[ip][iq]);
                }
            }
            if (sm == 0.0F)
            {
                break;
            }

            if (i < 3) // first 3 sweeps
            {
                tresh = 0.2F * sm / (n * n);
            }
            else
            {
                tresh = 0.0F;
            }

            for (ip = 0; ip < n - 1; ip++)
            {
                for (iq = ip + 1; iq < n; iq++)
                {
                    g = 100.0F * Mathf.Abs(a[ip][iq]);

                    // after 4 sweeps
                    if (i > 3 && (Mathf.Abs(w[ip]) + g) == Mathf.Abs(w[ip])
                    && (Mathf.Abs(w[iq]) + g) == Mathf.Abs(w[iq]))
                    {
                        a[ip][iq] = 0.0F;
                    }
                    else if (Mathf.Abs(a[ip][iq]) > tresh)
                    {
                        h = w[iq] - w[ip];
                        if ((Mathf.Abs(h) + g) == Mathf.Abs(h))
                        {
                            t = (a[ip][iq]) / h;
                        }
                        else
                        {
                            theta = 0.5F * h / (a[ip][iq]);
                            t = 1.0F / (Mathf.Abs(theta) + Mathf.Sqrt(1.0F + theta * theta));
                            if (theta < 0.0F)
                            {
                                t = -t;
                            }
                        }
                        c = 1.0F / Mathf.Sqrt(1 + t * t);
                        s = t * c;
                        tau = s / (1.0F + c);
                        h = t * a[ip][iq];
                        z[ip] -= h;
                        z[iq] += h;
                        w[ip] -= h;
                        w[iq] += h;
                        a[ip][iq] = 0.0F;

                        // ip already shifted left by 1 unit
                        for (j = 0; j <= ip - 1; ++j)
                        {
                            g = a[j][ip];
                            h = a[j][iq];
                            a[j][ip] = g - s * (h + g * tau);
                            a[j][iq] = h + s * (g - h * tau);
                        }
                        // ip and iq already shifted left by 1 unit
                        for (j = ip + 1; j <= iq - 1; ++j)
                        {
                            g = a[ip][j];
                            h = a[j][iq];
                            a[ip][j] = g - s * (h + g * tau);
                            a[j][iq] = h + s * (g - h * tau);
                        }
                        // iq already shifted left by 1 unit
                        for (j = iq + 1; j < n; ++j)
                        {
                            g = a[ip][j];
                            h = a[iq][j];
                            a[ip][j] = g - s * (h + g * tau);
                            a[iq][j] = h + s * (g - h * tau);
                        }
                        for (j = 0; j < n; ++j)
                        {
                            g = v[j][ip];
                            h = v[j][iq];
                            v[j][ip] = g - s * (h + g * tau);
                            v[j][iq] = h + s * (g - h * tau);
                        }
                    }
                }
            }

            for (ip = 0; ip < n; ip++)
            {
                b[ip] += z[ip];
                w[ip] = b[ip];
                z[ip] = 0.0F;
            }
        }



        // sort eigenfunctions                 these changes do not affect accuracy
        for (j = 0; j < n - 1; ++j)                  // boundary incorrect
        {
            k = j;
            tmp = w[k];
            for (i = j + 1; i < n; ++i)                // boundary incorrect, shifted already
            {
                if (w[i] >= tmp)                   // why exchange if same?
                {
                    k = i;
                    tmp = w[k];
                }
            }
            if (k != j)
            {
                w[k] = w[j];
                w[j] = tmp;
                for (i = 0; i < n; ++i)
                {
                    tmp = v[i][j];
                    v[i][j] = v[i][k];
                    v[i][k] = tmp;
                }
            }
        }
        // insure eigenvector consistency (i.e., Jacobi can compute vectors that
        // are negative of one another (.707,.707,0) and (-.707,-.707,0). This can
        // reek havoc in hyperstreamline/other stuff. We will select the most
        // positive eigenvector.
        int ceil_half_n = (n >> 1) + (n & 1);
        for (j = 0; j < n; ++j)
        {
            for (numPos = 0, i = 0; i < n; ++i)
            {
                if (v[i][j] >= 0.0F)
                {
                    numPos++;
                }
            }
            if (numPos < ceil_half_n)
            {
                for (i = 0; i < n; ++i)
                {
                    v[i][j] *= -1.0F;
                }
            }
        }

        /*if (n > 4)
        {
            delete[] b;
            delete[] z;
        }*/
        return true;
    }

    // Code adapted from C++ vtkMath.cxx --------------------------------------------------
    // Given a unit vector v1, find two other unit vectors v2 and v3 which
    // which form an orthonormal set.
    static void Perpendiculars(float[] v1, float[] v2, float[] v3, float theta)
    {
        float v1sq = v1[0] * v1[0];
        float v2sq = v1[1] * v1[1];
        float v3sq = v1[2] * v1[2];
        float r = Mathf.Sqrt(v1sq + v2sq + v3sq);

        // transpose the vector to avoid divide-by-zero error
        int dv1, dv2, dv3;
        if (v1sq > v2sq && v1sq > v3sq)
        {
            dv1 = 0; dv2 = 1; dv3 = 2;
        }
        else if (v2sq > v3sq)
        {
            dv1 = 1; dv2 = 2; dv3 = 0;
        }
        else
        {
            dv1 = 2; dv2 = 0; dv3 = 1;
        }

        float a = v1[dv1] / r;
        float b = v1[dv2] / r;
        float c = v1[dv3] / r;

        float tmp = Mathf.Sqrt(a * a + c * c);

        if (theta != 0.0F)
        {
            float sintheta = Mathf.Sin(theta);
            float costheta = Mathf.Cos(theta);

            if (v2 != null)
            {
                v2[dv1] = (c * costheta - a * b * sintheta) / tmp;
                v2[dv2] = sintheta * tmp;
                v2[dv3] = (-a * costheta - b * c * sintheta) / tmp;
            }

            if (v3 != null)
            {
                v3[dv1] = (-c * sintheta - a * b * costheta) / tmp;
                v3[dv2] = costheta * tmp;
                v3[dv3] = (a * sintheta - b * c * costheta) / tmp;
            }
        }
        else
        {
            if (v2 != null)
            {
                v2[dv1] = c / tmp;
                v2[dv2] = 0;
                v2[dv3] = -a / tmp;
            }

            if (v3 != null)
            {
                v3[dv1] = -a * b / tmp;
                v3[dv2] = tmp;
                v3[dv3] = -b * c / tmp;
            }
        }
    }

    // Matrices helper methods.
    // Original code by Raudel Ravelo : 
    // https://stackoverflow.com/questions/46836908/double-inversion-c-sharp
    static float[][] MatrixCreate(int rows, int cols)
    {
        float[][] result = new float[rows][];
        for (int i = 0; i < rows; ++i)
            result[i] = new float[cols];
        return result;
    }

    static float[][] MatrixIdentity(int n)
    {
        // return an n x n Identity matrix
        float[][] result = MatrixCreate(n, n);
        for (int i = 0; i < n; ++i)
            result[i][i] = 1.0F;

        return result;
    }

    static float[][] MatrixProduct(float[][] matrixA, float[][] matrixB)
    {
        int aRows = matrixA.Length; int aCols = matrixA[0].Length;
        int bRows = matrixB.Length; int bCols = matrixB[0].Length;
        if (aCols != bRows)
            throw new Exception("Non-conformable matrices in MatrixProduct");

        float[][] result = MatrixCreate(aRows, bCols);

        for (int i = 0; i < aRows; ++i) // each row of A
            for (int j = 0; j < bCols; ++j) // each col of B
                for (int k = 0; k < aCols; ++k) // could use k less-than bRows
                    result[i][j] += matrixA[i][k] * matrixB[k][j];

        return result;
    }

    static float[][] MatrixInverse(float[][] matrix)
    {
        int n = matrix.Length;
        float[][] result = MatrixDuplicate(matrix);

        int[] perm;
        int toggle;
        float[][] lum = MatrixDecompose(matrix, out perm,
          out toggle);
        if (lum == null)
            throw new Exception("Unable to compute inverse");

        float[] b = new float[n];
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (i == perm[j])
                    b[j] = 1.0F;
                else
                    b[j] = 0.0F;
            }

            float[] x = HelperSolve(lum, b);

            for (int j = 0; j < n; ++j)
                result[j][i] = x[j];
        }
        return result;
    }

    static float[][] MatrixDuplicate(float[][] matrix)
    {
        // allocates/creates a duplicate of a matrix.
        float[][] result = MatrixCreate(matrix.Length, matrix[0].Length);
        for (int i = 0; i < matrix.Length; ++i) // copy the values
            for (int j = 0; j < matrix[i].Length; ++j)
                result[i][j] = matrix[i][j];
        return result;
    }

    static float[] HelperSolve(float[][] luMatrix, float[] b)
    {
        // before calling this helper, permute b using the perm array
        // from MatrixDecompose that generated luMatrix
        int n = luMatrix.Length;
        float[] x = new float[n];
        b.CopyTo(x, 0);

        for (int i = 1; i < n; ++i)
        {
            float sum = x[i];
            for (int j = 0; j < i; ++j)
                sum -= luMatrix[i][j] * x[j];
            x[i] = sum;
        }

        x[n - 1] /= luMatrix[n - 1][n - 1];
        for (int i = n - 2; i >= 0; --i)
        {
            float sum = x[i];
            for (int j = i + 1; j < n; ++j)
                sum -= luMatrix[i][j] * x[j];
            x[i] = sum / luMatrix[i][i];
        }

        return x;
    }

    static float[][] MatrixDecompose(float[][] matrix, out int[] perm, out int toggle)
    {
        // Doolittle LUP decomposition with partial pivoting.
        // rerturns: result is L (with 1s on diagonal) and U;
        // perm holds row permutations; toggle is +1 or -1 (even or odd)
        int rows = matrix.Length;
        int cols = matrix[0].Length; // assume square
        if (rows != cols)
            throw new Exception("Attempt to decompose a non-square m");

        int n = rows; // convenience

        float[][] result = MatrixDuplicate(matrix);

        perm = new int[n]; // set up row permutation result
        for (int i = 0; i < n; ++i) { perm[i] = i; }

        toggle = 1; // toggle tracks row swaps.
                    // +1 -greater-than even, -1 -greater-than odd. used by MatrixDeterminant

        for (int j = 0; j < n - 1; ++j) // each column
        {
            float colMax = Mathf.Abs(result[j][j]); // find largest val in col
            int pRow = j;
            //for (int i = j + 1; i less-than n; ++i)
            //{
            //  if (result[i][j] greater-than colMax)
            //  {
            //    colMax = result[i][j];
            //    pRow = i;
            //  }
            //}

            // reader Matt V needed this:
            for (int i = j + 1; i < n; ++i)
            {
                if (Mathf.Abs(result[i][j]) > colMax)
                {
                    colMax = Mathf.Abs(result[i][j]);
                    pRow = i;
                }
            }
            // Not sure if this approach is needed always, or not.

            if (pRow != j) // if largest value not on pivot, swap rows
            {
                float[] rowPtr = result[pRow];
                result[pRow] = result[j];
                result[j] = rowPtr;

                int tmp = perm[pRow]; // and swap perm info
                perm[pRow] = perm[j];
                perm[j] = tmp;

                toggle = -toggle; // adjust the row-swap toggle
            }

            // --------------------------------------------------
            // This part added later (not in original)
            // and replaces the 'return null' below.
            // if there is a 0 on the diagonal, find a good row
            // from i = j+1 down that doesn't have
            // a 0 in column j, and swap that good row with row j
            // --------------------------------------------------

            if (result[j][j] == 0.0)
            {
                // find a good row to swap
                int goodRow = -1;
                for (int row = j + 1; row < n; ++row)
                {
                    if (result[row][j] != 0.0)
                        goodRow = row;
                }

                if (goodRow == -1)
                    throw new Exception("Cannot use Doolittle's method");

                // swap rows so 0.0 no longer on diagonal
                float[] rowPtr = result[goodRow];
                result[goodRow] = result[j];
                result[j] = rowPtr;

                int tmp = perm[goodRow]; // and swap perm info
                perm[goodRow] = perm[j];
                perm[j] = tmp;

                toggle = -toggle; // adjust the row-swap toggle
            }
            // --------------------------------------------------
            // if diagonal after swap is zero . .
            //if (Mathf.Abs(result[j][j]) less-than 1.0E-20) 
            //  return null; // consider a throw

            for (int i = j + 1; i < n; ++i)
            {
                result[i][j] /= result[j][j];
                for (int k = j + 1; k < n; ++k)
                {
                    result[i][k] -= result[i][j] * result[j][k];
                }
            }


        } // main j column loop

        return result;
    }
    //---------------------------------------------------------------------------------------
    

    void alignmentTest()
    {
        GameObject sourceSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sourceSphere.name = "sourceSphere";
        sourceSphere.transform.position = new Vector3(24.8f, 15.7f, 135.0f);

        GameObject sourceSphere1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sourceSphere1.name = "sourceSphere1";
        sourceSphere1.transform.position = new Vector3(19.1f, 19.8f, 135.0f);

        GameObject sourceSphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sourceSphere2.name = "sourceSphere2";
        sourceSphere2.transform.position = new Vector3(15.0f, 14.0f, 141.0f);

        GameObject sourceSphere3 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sourceSphere3.name = "sourceSphere3";
        sourceSphere3.transform.position = new Vector3(15.0f, 14.0f, 135.0f);


        GameObject targetSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        targetSphere.name = "targetSphere";
        targetSphere.transform.position = new Vector3(19.1f, 28.7f, 115.9f);

        GameObject targetSphere1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        targetSphere1.name = "targetSphere1";
        targetSphere1.transform.position = new Vector3(20.3f, 30.8f, 122.6f);

        GameObject targetSphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        targetSphere2.name = "targetSphere2";
        targetSphere2.transform.position = new Vector3(22.2f, 22.0f, 125.0f);

        GameObject targetSphere3 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        targetSphere3.name = "targetSphere3";
        targetSphere3.transform.position = new Vector3(17.0f, 25.0f, 125.0f);


        List<Vector3> sourcePoints = new List<Vector3>();
        List<Vector3> targetPoints = new List<Vector3>();

        sourcePoints.Add(sourceSphere.transform.position);
        sourcePoints.Add(sourceSphere1.transform.position);
        sourcePoints.Add(sourceSphere2.transform.position);
        sourcePoints.Add(sourceSphere3.transform.position);
        Debug.Log("sourceSphere  : " + sourceSphere.transform.position);
        Debug.Log("sourceSphere1  : " + sourceSphere1.transform.position);
        Debug.Log("sourceSphere2  : " + sourceSphere2.transform.position);
        Debug.Log("sourceSphere3  : " + sourceSphere3.transform.position);

        targetPoints.Add(targetSphere.transform.position);
        targetPoints.Add(targetSphere1.transform.position);
        targetPoints.Add(targetSphere2.transform.position);
        targetPoints.Add(targetSphere3.transform.position);
        Debug.Log("targetSphere  : " + targetSphere.transform.position);
        Debug.Log("targetSphere1  : " + targetSphere1.transform.position);
        Debug.Log("targetSphere2  : " + targetSphere2.transform.position);
        Debug.Log("targetSphere3  : " + targetSphere3.transform.position);

        Matrix4x4 calibration = landMarkTransform(sourcePoints, targetPoints);
        Debug.Log("RESULT Calib :" + calibration);
        GameObject sphereResult = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        GameObject sphereResult1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        GameObject sphereResult2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        GameObject sphereResult3 = GameObject.CreatePrimitive(PrimitiveType.Sphere);

        sphereResult.transform.position = sourceSphere.transform.position;
        sphereResult.transform.rotation = sourceSphere.transform.rotation;
        sphereResult1.transform.position = sourceSphere1.transform.position;
        sphereResult1.transform.rotation = sourceSphere1.transform.rotation;
        sphereResult2.transform.position = sourceSphere2.transform.position;
        sphereResult2.transform.rotation = sourceSphere2.transform.rotation;
        sphereResult3.transform.position = sourceSphere3.transform.position;
        sphereResult3.transform.rotation = sourceSphere3.transform.rotation;

        sphereResult.GetComponent<Renderer>().material = resultMaterial;

        Matrix4x4 sphereTransMat = calibration * sphereResult.transform.worldToLocalMatrix.inverse;
        Matrix4x4 sphereTransMat1 = calibration * sphereResult1.transform.worldToLocalMatrix.inverse;
        Matrix4x4 sphereTransMat2 = calibration * sphereResult2.transform.worldToLocalMatrix.inverse;
        Matrix4x4 sphereTransMat3 = calibration * sphereResult3.transform.worldToLocalMatrix.inverse;

        sphereResult.transform.rotation = new Quaternion(sphereTransMat.inverse.rotation.x, sphereTransMat.inverse.rotation.y, sphereTransMat.inverse.rotation.z, sphereTransMat.inverse.rotation.w);
        sphereResult.transform.position = new Vector3(sphereTransMat.GetColumn(3).x, sphereTransMat.GetColumn(3).y, sphereTransMat.GetColumn(3).z);

        sphereResult1.transform.rotation = new Quaternion(sphereTransMat1.inverse.rotation.x, sphereTransMat1.inverse.rotation.y, sphereTransMat1.inverse.rotation.z, sphereTransMat1.inverse.rotation.w);
        sphereResult1.transform.position = new Vector3(sphereTransMat1.GetColumn(3).x, sphereTransMat1.GetColumn(3).y, sphereTransMat1.GetColumn(3).z);

        sphereResult2.transform.rotation = new Quaternion(sphereTransMat2.inverse.rotation.x, sphereTransMat2.inverse.rotation.y, sphereTransMat2.inverse.rotation.z, sphereTransMat2.inverse.rotation.w);
        sphereResult2.transform.position = new Vector3(sphereTransMat2.GetColumn(3).x, sphereTransMat2.GetColumn(3).y, sphereTransMat2.GetColumn(3).z);

        sphereResult3.transform.rotation = new Quaternion(sphereTransMat3.inverse.rotation.x, sphereTransMat3.inverse.rotation.y, sphereTransMat3.inverse.rotation.z, sphereTransMat3.inverse.rotation.w);
        sphereResult3.transform.position = new Vector3(sphereTransMat3.GetColumn(3).x, sphereTransMat3.GetColumn(3).y, sphereTransMat3.GetColumn(3).z);

        Debug.Log(sphereTransMat);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="patternW">width of the pattern attached to the board in mm</param>
    /// <param name="depth">the depth of the of the points with respect to the z-axis of the coordinate system of the pattern</param>
    /// <param name="boardW">width of the board in mm</param>
    /// <returns></returns>
    public List<Vector3> createBoardPoints(float patternW, float depth, float boardW)
    {
        List<Vector3> boardPoints = new List<Vector3>();

        float step = 20.0F;
        for (float j = -boardW / 2; j <= boardW / 2; j += step)
        {
            for (float i = -patternW / 2; i <= boardW - patternW / 2; i += step)
            {


                if (!(i >= -patternW / 2 && i <= patternW / 2 && j >= -40 && j <= 40))
                {
                    boardPoints.Add(new Vector3(j / patternW, depth / patternW, -i / patternW));
                }
            }

        }
        return boardPoints;
    }

    /// <summary>
    /// Creates the ground truth for the 2D pattern.
    /// </summary>
    /// <param name="patternW">width of the pattern in mm</param>
    /// <param name="depth">depth of the pattern object in mm</param>
    /// <returns></returns>
    public List<Vector3> createPatternPoints(float patternW, float depth)
    {
        List<Vector3> patternPoints = new List<Vector3>();

        float step = 10.0F;
        for (float j = -patternW / 2 + 10.0F; j < patternW / 2; j += step)
        {
            for (float i = -patternW / 2 + 10.0F; i < patternW / 2; i += step)
            {
                patternPoints.Add(new Vector3(j/ patternW, depth / patternW, -i / patternW));
            }

        }
        return patternPoints;
    }

    /// <summary>
    /// Extracts a rectangular pattern of points from the board.
    /// </summary>
    /// <param name="boardPoints">the board points</param>
    /// <param name="level"> this defines the starting divot of the pattern
    /// 0: outer upper point<para/>
    /// 8: inner upper point <para/>
    /// The higher the level the inner the starting point is : the closest it is to the marker</param>
    /// <param name="n_rows">number of rows on the board : 21</param>
    /// <param name="step">This parameter determines the number of points to pick based on the colums to jump <para/>
    /// 1: every point<para/>
    /// 2: take 1 row/col and ignore the following one <para/>
    /// 3: take 1 row/col and ignore the 2 following ones<para/>
    /// 7: would take the most outer points</param>
    /// <param name="n_patternRows">The number of the hidden divots on the board that are under the pattern</param>
    /// <returns></returns>
    public List<Vector3> boardBorderPointExtract(List<Vector3> boardPoints, int level, int n_rows, int step, int n_patternRows)
    {
        List<Vector3> borderPoints = new List<Vector3>();
        // 3 levels on pattern
        int startIndex = level * n_rows;
        int endPointrow = n_rows - level * 2 - 1;
        int numberOfHiddenCols = 5;
        int endPointIndex = getIndex(endPointrow, endPointrow, n_rows) - n_patternRows + level * n_rows; //the bottom right point
        int endPointcolindex = endPointIndex - (n_rows - level * 2) + 1;
        // get borders of the pattern
        // top row
        int numberOfHorizontalDivots = 0;
        Debug.Log(endPointcolindex);
        for (int i = startIndex; i <= endPointcolindex; i += n_rows * step)
        {
            Debug.Log(i);
            // if (i == n_rows * 8)
            // Define number of columns to avoid. They are hidden under the marker.
            // Only 8 rows from the left and right of the pattern are visible.
            // Jump necessary lines to keep symmetry with respect to the y-axis of the pattern.
            int val = i == n_rows * 8 ? numberOfHiddenCols : //jump all the hidden cols
               i - numberOfHiddenCols == (n_rows * 8 + (n_rows - numberOfHiddenCols)) ? 4 :
               i - numberOfHiddenCols * 2 == (n_rows * 8 + (n_rows - numberOfHiddenCols) * 2) ? 3 :
               i - numberOfHiddenCols * 3 == (n_rows * 8 + (n_rows - numberOfHiddenCols) * 3) ? 2 :
               i - numberOfHiddenCols * 4 == (n_rows * 8 + (n_rows - numberOfHiddenCols) * 4) ? 1 : 0;

            if (val != 0)
            {
                int symmetric = (7 - (i - step * n_rows) / n_rows) * n_rows;// if (level != 0) symmetric-=level;
                i = i - (numberOfHiddenCols - val) * numberOfHiddenCols //index of point from where to start jumping
                    + (n_rows - numberOfHiddenCols) * val // hidden divots to jump
                    + symmetric; // to keep the symmetry
            }
            
            borderPoints.Add(boardPoints[i]);
            if (step == 20 || step == 10) i -= 25;
            //if (step == 10) i -= 25;
            numberOfHorizontalDivots++;
        }

        // right column
        for (int i = endPointcolindex + step; i < endPointIndex; i += step)
        {
            borderPoints.Add(boardPoints[i]);
        }

        // bottom row
        int bottomEndPointIndex = startIndex + (n_rows - level * 2) - 1;
        int size = borderPoints.Count;
        //borderPoints.Insert(size, boardPoints[bottomEndPointIndex]);
        int minus = 0;
        for (int i = startIndex + (n_rows - (n_rows - endPointrow)) + n_rows * step; i <= endPointIndex + numberOfHiddenCols * 5; i += n_rows * step)
        {
            int n = i / n_rows;
            switch (n)
            {
                case int s when (s >= 0 & s < 8):
                    minus = 0;
                    break;
                case 8:
                    minus = numberOfHiddenCols * 1;
                    break;
                case 9:
                    minus = numberOfHiddenCols * 2;
                    break;
                case 10:
                    minus = numberOfHiddenCols * 3;
                    break;
                case 11:
                    minus = numberOfHiddenCols * 4;
                    break;
                case 12:
                    minus = numberOfHiddenCols * 5;
                    break;
                case int s when (s >= 13):
                    minus = numberOfHiddenCols * 5;
                    break;

            }

            borderPoints.Insert(size, boardPoints[i - minus]);

        }

        // left column
        for (int i = bottomEndPointIndex; i > startIndex; i -= step)
        {
            borderPoints.Add(boardPoints[i]);
        }

        return borderPoints;
    }





    /// <summary>
    /// Extracts points from the full points list on the 2D pattern
    /// </summary>
    /// <param name="patternPoints"></param>
    /// <param name="level">
    /// 0: most outer square<para/>
    /// 1: middle square <para/>
    /// Else: most inner square</param>
    /// <param name="n_rows">
    /// 7: number of rows on the pattern</param>
    /// <param name="step">This parameter determines the number of points to pick based on the colums to jump <para/>
    /// 1: every point<para/>
    /// 2: take 1 row/col and ignore the following one <para/>
    /// 3: take 1 row/col and ignore the 2 following ones<para/>
    /// 7: would take the most outer points</param>
    /// <returns></returns>
    public List<Vector3> patternBorderPointExtract(List<Vector3> patternPoints, int level, int n_rows, int step)
    {
        List<Vector3> borderPoints = new List<Vector3>();
        // 3 levels on pattern
        int startIndex = level == 0 ? 0 : level == 1 ? 8 : 16;
        int endPointrow = level == 0 ? 6 : level == 1 ? 5 : 4;

        int endPointIndex = getIndex(endPointrow, endPointrow, n_rows); //the bottom right point
        int endPointcolindex = endPointIndex - (n_rows - level * 2);
        // get borders of the pattern
        // top row
        for (int i = startIndex; i < endPointcolindex; i += n_rows * step)
        {
            borderPoints.Add(patternPoints[i]);
        }

        // right column
        for (int i = endPointcolindex + 1; i < endPointIndex; i += step)
        {
            borderPoints.Add(patternPoints[i]);
        }

        // bottom row
        for (int i = endPointIndex; i > startIndex + (n_rows - level * 2); i -= n_rows * step)
        {
            borderPoints.Add(patternPoints[i]);
        }

        // left column
        for (int i = startIndex + (n_rows - level * 2) - 1; i > startIndex; i -= step)
        {
            borderPoints.Add(patternPoints[i]);
        }

        return borderPoints;
    }

    int getIndex(int rowNumber, int colNumber, int n_rows)
    {
        return (colNumber * n_rows) + rowNumber;
    }



    /// <summary>
    /// Tests 2 methods for extracting points from the Board and the pattern ground truth (divots)
    /// The methods are : <c>patternBorderPointExtract</c> and <c>boardBorderPointExtract</c>
    /// </summary>
    /// <returns></returns>
    public List<Vector3> pointsExtractorTest()
    {
        List<Vector3> calibrationPoints = createBoardPoints(Constants.MARKER_SIZE, -5.0F, 400.0F);
        List<Vector3> patternPoints = createPatternPoints(Constants.MARKER_SIZE, 0.0F);
        Debug.Log("n=" + patternPoints.Count);
        Debug.Log("n=" + calibrationPoints.Count);

        List<Vector3> borderPoints = patternBorderPointExtract(patternPoints, 0, 7, 7); //level 2 : outer border
        List<Vector3> boardBorderPoints;
        boardBorderPoints = new List<Vector3>();

        calibrationPoints.AddRange(boardBorderPoints);
        calibrationPoints.AddRange(patternBorderPointExtract(patternPoints, 0, 7, 1));

        boardBorderPoints.AddRange(calibrationPoints);
        Debug.Log("n of points = " + boardBorderPoints.Count);
        for (int i = 0; i < boardBorderPoints.Count; i++)
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            go.name = ("Sphere" + i);
            go.transform.localScale = new Vector3(0.1F, 0.1F, 0.1F);
            go.transform.position = boardBorderPoints[i];
        }

        return calibrationPoints;
    }

}